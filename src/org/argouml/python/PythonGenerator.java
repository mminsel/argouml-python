// $Id: PythonGenerator.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package org.argouml.python;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.argouml.python.transform.Model2SourceUnits;
import org.argouml.uml.generator.CodeGenerator;
import org.argouml.uml.generator.SourceUnit;

public class PythonGenerator implements CodeGenerator 
{
    private final Logger log;
    
    public PythonGenerator() 
    {
        this.log = Logger.getLogger(this.getClass());
    }

    public Collection generate(Collection objs, boolean deps) {
        Model2SourceUnits t = new Model2SourceUnits("", deps);

        try
        {
            return t.toSourceUnits(objs);
        }
        catch (Exception e)
        {
            this.log.error("while transforming: " + e);
            return null;
        }

    }

    public Collection generateFiles(Collection objs, String path, boolean deps) 
    {
        Collection<String> res = new ArrayList<String>();
        Model2SourceUnits t = new Model2SourceUnits(path, deps);
        Collection<SourceUnit> sus;

        // transform
        try
        {
            sus = t.toSourceUnits(objs);
        }
        catch (Exception e)
        {
            this.log.error("while transforming: " + e);
            return null;
        }

        for (SourceUnit su : sus) {
            try 
            {
                // create directory
                File dir = new File(su.getBasePath());
                dir.mkdirs();

                // create file
                File file = new File(su.getFullName());

                // write file
                OutputStream os = new FileOutputStream(file);
                OutputStreamWriter osw = new OutputStreamWriter(os);
                osw.write(su.getContent());
                osw.close();
                res.add(file.getName());
            } 
            catch (IOException e) 
            {
                this.log.error("can't create file '" 
                        + su.getFullName() + "': " + e);
            }
        }        

        return res;
    }

    public Collection generateFileList(Collection objs, boolean deps) 
    {
        Collection<String> res = new ArrayList<String>();
        Model2SourceUnits t = new Model2SourceUnits("", deps);
        Collection<SourceUnit> sus;

        // transform
        try
        {
            sus = t.toSourceUnits(objs);
        }
        catch (Exception e)
        {
            this.log.error("while transforming: " + e);
            return null;
        }

        for (SourceUnit su : sus) {
            res.add(su.getFullName());
        }        

        return res;
    }
}
