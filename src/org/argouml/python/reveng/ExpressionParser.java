// $Id: ExpressionParser.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, 
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.argouml.python.reveng;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;
import org.argouml.python.py.PyExpression;

public class ExpressionParser
{
    public static class MyLexer extends PythonLexer
    {
        public MyLexer(CharStream lexer)
        {
            super(lexer);
        }

        public Token nextToken()
        {
            startPos = getCharPositionInLine();
            return super.nextToken();
        }
    }

    public PyExpression toExpression(String s) throws RecognitionException
    {
        // dirty hack to make this work :\
        // copies whole string, but an expression shouldn't be that big
        if (!s.endsWith("\n"))
        {
            s += "\n";
        }
        
        CharStream input;
        input = new ANTLRStringStream(s);
        

        PythonLexer lexer = new MyLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        tokens.discardOffChannelTokens(true);
        PythonTokenSource indentedSource = new PythonTokenSource(tokens);
        tokens = new CommonTokenStream(indentedSource);
        PythonParser parser = new PythonParser(tokens);
        PythonParser.expr_stmt_return r;

        r = parser.expr_stmt();

        CommonTree r0 = ((CommonTree) r.tree);
        CommonTreeNodeStream nodes = new CommonTreeNodeStream(r0);
        nodes.setTokenStream(tokens);
        PythonWalker walker = new PythonWalker(nodes);

        return walker.expr_stmt();
    }
}
