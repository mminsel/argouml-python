// $Id: File2CompilationUnit.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.reveng;

import java.io.File;
import java.io.IOException;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;
import org.apache.log4j.Logger;
import org.argouml.python.py.PyCompilationUnit;

public class File2CompilationUnit
{
    private static final Logger LOG = Logger
    .getLogger(File2CompilationUnit.class);

    public static class MyLexer extends PythonLexer
    {
        public MyLexer(CharStream lexer)
        {
            super(lexer);
        }

        public Token nextToken()
        {
            startPos = getCharPositionInLine();
            return super.nextToken();
        }
    }

    public PyCompilationUnit toCompilationUnit(File f)
    {
        CharStream input;
        try
        {
            input = new ANTLRFileStream(f.toString());
        }
        catch (IOException e)
        {
            LOG.error("can't read file '" + f.toString() + "': " + e);
            return null;
        }

        PythonLexer lexer = new MyLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        tokens.discardOffChannelTokens(true);
        PythonTokenSource indentedSource = new PythonTokenSource(tokens);
        tokens = new CommonTokenStream(indentedSource);
        PythonParser parser = new PythonParser(tokens);
        PythonParser.file_input_return r;

        try
        {
            r = parser.file_input();
        }
        catch (RecognitionException e)
        {
            LOG.error("can't parse file '" + f.toString() + "': " + e);
            return null;
        }

        CommonTree r0 = ((CommonTree) r.tree);
        CommonTreeNodeStream nodes = new CommonTreeNodeStream(r0);
        nodes.setTokenStream(tokens);
        PythonWalker walker = new PythonWalker(nodes);

        try
        {
            return walker.module(f);
        }
        catch (RecognitionException e)
        {
            LOG.error("walker can't parse file '" + f.toString() + "': " + e);
            return null;
        }
    }
}
