/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

import java.io.File;
import java.util.ArrayList;

public class PyCompilationUnit
{
    private File file;
    private ArrayList<PyStatement> statements = new ArrayList<PyStatement>();

    public PyCompilationUnit(File file)
    {
        super();
        this.file = file;
    }

    public String getName()
    {
        return this.file.toString();
    }

    public File getFile()
    {
        return this.file;
    }

    public String getModuleName()
    {
        String filename = this.file.getName();

        int i1 = filename.indexOf(".");
        if (i1 < 0)
        {
            i1 = filename.length();
        }

        System.out.println("module-name: " + filename.substring(0, i1));
        return filename.substring(0, i1);

    }

    public void addStatement(PyStatement s)
    {
        if (s != null)
        {
            this.statements.add(s);
        }
    }

    public void addStatements(ArrayList<PyStatement> stmts)
    {
        if (stmts == null)
        {
            return;
        }

        for (PyStatement s : stmts)
        {
            this.addStatement(s);
        }
    }

    public ArrayList<PyStatement> getStatements()
    {
        return this.statements;
    }

    public ArrayList<PyClassDefinition> getClassDefinitions()
    {
        ArrayList<PyClassDefinition> res = new ArrayList<PyClassDefinition>();
        for (PyStatement s : this.statements)
        {
            if (s instanceof PyClassDefinition)
            {
                res.add((PyClassDefinition) s);
            }
        }
        return res;
    }

    public String toString()
    {
        return "[PyCompilationUnit '" + this.file + "', "
        + this.statements.size() + " statements]";
    }
}
