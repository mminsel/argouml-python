// $Id: PyModule.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/


package org.argouml.python.py;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class PyModule extends PyElement
{
	ArrayList<PyStatement> body = new ArrayList<PyStatement>();
	
    public PyModule(String name, PyModule parent)
    {
        super("module", name, parent);
    }

    public PyModule(String name)
    {
        this(name, null);
    }

    public Set<PyModule> getSubModules()
    {
        Set<PyModule> res = new HashSet<PyModule>();
        for (PyElement e : getChildren()) {
            if ("modules".equals(e.getType()))
            {
                res.add((PyModule) e);
            }
        }

        return res;
    }

    public PyModule getSubModule(String name)
    {
        for (PyModule module : getSubModules()) {
            if (name.equals(module.getName()))
            {
                return module;
            }
        }

        return null;
    }

    public String getPath()
    {
        String fs = System.getProperty("file.separator");

        if (getParent() == null || getParent().getType() != "module")
        {
            return getName();
        }
        else
        {
            return ((PyModule) getParent()).getPath() + fs + getName();
        }
    }
}
