// $Id: PyReturnStatement.java 93 2010-01-12 19:40:42Z linus $ 
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

public class PyReturnStatement extends PyExpression
{
    private PyExpression value;
    private String valueS = "";

    public PyReturnStatement()
    {
        super();
        this.value = null;
    }

    public String getValue()
    {
        if (this.value != null)
        {
            return this.value.toString();
        }
        return this.valueS;
    }

    public void setValue(PyExpression value)
    {
        this.value = value;
    }
    
    public void setValue(String value)
    {
        this.valueS = value;
    }

    @Override
    public String toCode()
    {
        if (this.value == null)
        {
            if (!this.valueS.isEmpty())
            {
                return "return " + this.valueS;
            }
            return "pass";
        }
        else if (this.value != null)
        {
            return "return " + this.value.toString();
        }
        return "";
    }
}
