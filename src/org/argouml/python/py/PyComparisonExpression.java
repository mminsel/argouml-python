/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

import java.util.ArrayList;

public class PyComparisonExpression extends PyExpression
{
    private PyExpression.ComparisonOperator operator;
    private ArrayList<PyExpression> operands = new ArrayList<PyExpression>();

    public PyComparisonExpression(PyExpression.ComparisonOperator operator)
    {
        this.operator = operator;
    }

    public PyComparisonExpression()
    {
        this(null);
    }

    public void setOperator(PyExpression.ComparisonOperator operator)
    {
        this.operator = operator;
    }

    public PyExpression.ComparisonOperator getOperator()
    {
        return this.operator;
    }

    public void addOperand(PyExpression operand)
    {
        this.operands.add(operand);
    }

    public PyExpression getOperand(int i)
    {
        return this.operands.get(i);
    }

    public ArrayList<PyExpression> getOperands()
    {
        return this.operands;
    }

    public String toCode()
    {
        StringBuffer tmp = new StringBuffer();

        String op = null;
        if (this.operator == null)
        {
            op = " ?? ";
        }
        else
        {
            op = " " + this.operator.toString() + " ";
        }

        boolean first = true;
        for (PyExpression o : this.operands)
        {
            if (first)
            {
                first = false;
            }
            else
            {
                tmp.append(op);
            }
            
            tmp.append(o.toString());
        }

        return tmp.toString();
    }
}