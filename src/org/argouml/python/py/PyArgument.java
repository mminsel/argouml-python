// $Id: PyArgument.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.argouml.python.py;

import java.util.ArrayList;

public class PyArgument
{
    public static final int NORMAL = 0;
    public static final int VARLENGTH = 1;
    public static final int KEYWORD = 2;
    public static final int TUPLE = 3;

    private String name;
    private PyType type;
    private PyExpression defaultValue;
    private int argumentType;

    // probably not a good way for doing this, since this member
    // is only relevant if argumentType == TUPLE
    private ArrayList<String> tupleNames = new ArrayList<String>();

    public PyArgument(String name, PyType type, int argumentType)
    {
        this.name = name;
        this.type = type;
        this.argumentType = argumentType;
    }

    public PyArgument(String name, int argumentType)
    {
        this(name, null, argumentType);
    }

    public PyArgument(String name)
    {
        this(name, null, PyArgument.NORMAL);
    }

    public PyArgument()
    {
        this("", null, PyArgument.NORMAL);
    }

    public int getArgumentType()
    {
        return argumentType;
    }

    public void setArgumentType(int argumentType)
    {
        this.argumentType = argumentType;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return this.name;
    }

    public PyType getType()
    {
        return this.type;
    }

    public PyExpression getDefaultValue()
    {
        return defaultValue;
    }

    public void setDefaultValue(PyExpression defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public ArrayList<String> getTupleNames()
    {
        return tupleNames;
    }

    public void addTupleName(String name)
    {
        if (name != null)
        {
            this.tupleNames.add(name);
        }
    }

    public void addTupleNames(ArrayList<String> names)
    {
        if (names == null)
        {
            return;
        }

        for (String name : names)
        {
            this.addTupleName(name);
        }
    }

    public String toString()
    {
        return this.toCode();
    }

    public String toCode()
    {
        StringBuffer tmp = new StringBuffer();

        if (this.argumentType == KEYWORD)
        {
            tmp.append("**");
        }
        else if (this.argumentType == VARLENGTH)
        {
            tmp.append("*");
        }
        else if (this.argumentType == TUPLE)
        {
            tmp.append("(");
        }

        tmp.append(this.name);

        if (this.defaultValue != null)
        {
            tmp.append(" = ");
            tmp.append(this.defaultValue.toString());
        }

        if (this.argumentType == TUPLE)
        {
            tmp.append(")");
        }

        return tmp.toString();
    }
}
