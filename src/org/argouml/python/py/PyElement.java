// $Id: PyElement.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/


package org.argouml.python.py;

import java.util.HashSet;
import java.util.Set;

/**
 * A Python element.  This class is the root of the hierarchy of types of 
 * Python elements (e.g. Class, Module, etc).
 * 
 * @author Alexander Krohn
 * 
 * TODO: Complete Javadoc.
 */
public class PyElement
{
    private String type;
    private String name;
    private String docstring;
    private PyElement parent;
    private Set<PyElement> children = new HashSet<PyElement>();

    public PyElement(String type, String name, PyElement parent)
    {
        this.type = type;
        this.name = name;
        this.parent = parent;
        this.docstring = null;
    }

    public PyElement(String type, String name)
    {
        this(type, name, null);
    }

    public PyElement(String type)
    {
        this(type, type, null);
    }

    public String getType()
    {
        return this.type;
    }

    protected void setType(String t)
    {
        this.type = t;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean hasDocstring()
    {
        return (this.docstring != null) && (this.docstring != "");
    }

    public void setDocstring(String docstring)
    {
        this.docstring = docstring;
    }

    public String getDocstring()
    {
        return this.docstring;
    }

    public PyElement getParent()
    {
        return this.parent;
    }

    protected void setParent(PyElement parent)
    {
        this.parent = parent;
    }

    public boolean hasParent()
    {
        return this.parent != null;
    }

    public void add(PyElement e)
    {
        e.setParent(this);
        this.children.add(e);
    }

    /**
     * @return the children of this element
     * @deprecated for 0.25.5 by tfmorris.  Use {@link #getChildren()}.
     */
    public Set<PyElement> getChilds()
    {
        return getChildren();
    }

    /**
     * @return the children of this element
     */
    public Set<PyElement> getChildren() {
        return this.children;
    }
    
    public String toString()
    {
        StringBuffer res = new StringBuffer();
        res.append("[PyElement, type: ");
        res.append(this.type);
        res.append(", name: ");
        res.append(this.name);
        res.append(", children: ");
        for (PyElement e : children) {
            res.append(e.toString());
        }
        
        res.append("]");

        return res.toString();
    }
}
