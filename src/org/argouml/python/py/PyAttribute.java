// $Id: PyAttribute.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package org.argouml.python.py;

public class PyAttribute extends PyVariable
{
    private boolean isPublic = true;
    private boolean isProtected = false;
    private boolean isPrivate = false;
    private boolean isClassAttribute = false;

    public PyAttribute(String name, String vartype, String initialValue, 
            PyElement parent) 
    {
        super(name, vartype, initialValue, parent);
        this.setType("attribute");
    }

    public PyAttribute(String name, String vartype, PyElement parent)
    {
        this(name, vartype, null, parent);
    }

    public PyAttribute(String name, String vartype)
    {
        this(name, vartype, null, null);
    }

    public PyAttribute(String name, PyElement parent)
    {
        this(name, null, null, parent);
    }

    public PyAttribute(String name)
    {
        this(name, null, null, null);
    }

    public boolean isPublic()
    {
        return this.isPublic;
    }

    public void isPublic(boolean b)
    {
        this.isPublic = b;
        if (b)
        {
            this.isPrivate = false;
            this.isProtected = false;
        }
    }

    public boolean isProtected()
    {
        return this.isProtected;
    }

    public void isProtected(boolean b)
    {
        this.isProtected = b;
        if (b)
        {
            this.isPublic = false;
            this.isPrivate = false;
        }
    }

    public boolean isPrivate()
    {
        return this.isPrivate;
    }

    public void isPrivate(boolean b)
    {
        this.isPrivate = b;
        if (b)
        {
            this.isPublic = false;
            this.isProtected = false;
        }
    }

    public boolean isClassAttribute()
    {
        return this.isClassAttribute;
    }

    public void isClassAttribute(boolean b)
    {
        this.isClassAttribute = b;
    }
}
