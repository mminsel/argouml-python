// $Id: PyExpression.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

public class PyExpression extends PyStatement
{
    private boolean isBraced;

    public enum UnaryOperator
    {
        INCREMENT, DECREMENT, LOGIC_NOT, BITWISE_NOT, PLUS, MINUS;
        public String toString()
        {
            switch (this)
            {
                case INCREMENT:
                    return "++";
                case DECREMENT:
                    return "--";
                case LOGIC_NOT:
                    return "not";
                case BITWISE_NOT:
                    return "~";
                case PLUS:
                    return "+";
                case MINUS:
                    return "+";
            }

            return "??";
        }
    }

    public enum BinaryOperator
    {
        ADD, SUBTRACT, MULTIPLY, DIVIDE, FLOOR_DIVIDE, POWER, MODULO, LOGIC_OR, 
        LOGIC_AND, LOGIC_XOR, BITWISE_OR, BITWISE_AND, BITWISE_XOR, RIGHTSHIFT, 
        LEFTSHIFT;
        public String toString()
        {
            switch (this)
            {
                case ADD:
                    return "+";
                case SUBTRACT:
                    return "-";
                case MULTIPLY:
                    return "*";
                case DIVIDE:
                    return "/";
                case FLOOR_DIVIDE:
                    return "//";
                case POWER:
                    return "**";
                case MODULO:
                    return "%";
                case LOGIC_OR:
                    return "or";
                case LOGIC_AND:
                    return "and";
                case LOGIC_XOR:
                    return "xor";
                case BITWISE_OR:
                    return "|";
                case BITWISE_AND:
                    return "&";
                case BITWISE_XOR:
                    return "^";
                case LEFTSHIFT:
                    return "<<";
                case RIGHTSHIFT:
                    return ">>";
            }

            return "??";
        }
    }

    public enum ComparisonOperator
    {
        GREATER, GREATER_EQUAL, LESS, LESS_EQUAL, EQUAL, NOT_EQUAL, IN, NOT_IN, 
        IS, IS_NOT;
        public String toString()
        {
            switch (this)
            {
                case GREATER:
                    return ">";
                case GREATER_EQUAL:
                    return ">=";
                case LESS:
                    return "<";
                case LESS_EQUAL:
                    return "<=";
                case NOT_EQUAL:
                    return "!=";
                case EQUAL:
                    return "==";
                case IN:
                    return "in";
                case NOT_IN:
                    return "not in";
                case IS:
                    return "is";
                case IS_NOT:
                    return "is not";
            }
            
            return "??";
        }
    }

    public enum TernaryOperator
    {
        IF_THEN_ELSE;
    }

    public PyExpression()
    {
        this.isBraced = false;
    }

    public PyExpression(boolean isBraced)
    {
        this.isBraced = isBraced;
    }

    public boolean isBraced()
    {
        return this.isBraced;
    }

    public void isBraced(boolean b)
    {
        this.isBraced = b;
    }

    // quite a hack :\
    public boolean isAtom()
    {
        return this instanceof PyAtom;
    }

    public String toString()
    {
        if (this.isBraced)
        {
            return "(" + this.toCode() + ")";
        }
        else
        {
            return this.toCode();
        }
    }

    public String toCode()
    {
        return "";
    }
}
