// $Id: PyFunctionDefinition.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

import java.util.ArrayList;

public class PyAssociationDefinition extends PyCompoundStatement
{

    /* multiplicity of an association defines what type it will be:
     * 
     * in case multiplicity is .. it will be converted to ..
     *      ONE: {CONSTRUCTOR}()
     *      NULL_OR_ONE: None # [TYPE]
     *      LIST_NO_MIN: []
     *      LIST_MIN_ONE: [{CONSTRUCTOR}()]
     * where {CONSTRUCTOR} is replaced with the childClass name
     *  */
    public enum multiplicityT {
            ONE, NULL_OR_ONE, LIST_NO_MIN, LIST_MIN_ONE, NONE
    };
    
    public static String multiplicityToString(multiplicityT c)
    {
        switch (c){
            case ONE:
                return "1";
            case NULL_OR_ONE:
                return "0..1";
            case LIST_NO_MIN:
                return "0..*";
            case LIST_MIN_ONE:
                return "1..*";
        }
        return "";
    }
    
    public static multiplicityT multiplicityFromString(String s)
    {
        for (multiplicityT c: multiplicityT.values())
        {
            if (s.contentEquals(multiplicityToString(c)))
            {
                return c;
            }
        }
        return multiplicityT.NONE;
    }
    
    private String name;
    private PyClassDefinition parent;
    private String child = "";
    private ArrayList<String> childNamespace = new ArrayList<String>();
    private multiplicityT multiplicity = multiplicityT.NONE;
    
    private ArrayList<PyImportStatement> imports =  new ArrayList<PyImportStatement>();
    private ArrayList<PyFunctionDefinition> getterAndSetter = new ArrayList<PyFunctionDefinition>();
    
    public PyAssociationDefinition(String name, PyClassDefinition parentClass)
    {
        this(name, parentClass, "", new ArrayList<String>(), multiplicityT.NONE);
    }
    
    public PyAssociationDefinition(String name, PyClassDefinition parentClass, multiplicityT multiplicity)
    {
        this(name, parentClass, "", new ArrayList<String>(), multiplicity);
    }
    
    public PyAssociationDefinition(String name, PyClassDefinition parentClass, String childClass, multiplicityT multiplicity)
    {
        this(name, parentClass, childClass, new ArrayList<String>(), multiplicity);
    }
    
    public PyAssociationDefinition(String name, PyClassDefinition parentClass, String childClass)
    {
        this(name, parentClass, childClass, new ArrayList<String>(), multiplicityT.NONE);
        
    }
    
    public PyAssociationDefinition(String name, PyClassDefinition parentClass, String childClass, ArrayList<String> ns)
    {
        
        this(name, parentClass, childClass, ns, multiplicityT.NONE);
    }
    
    public PyAssociationDefinition(String name, PyClassDefinition parentClass, String childClass, ArrayList<String> ns, multiplicityT multi)
    {
        this.name = name;
        this.parent = parentClass;
        this.child = childClass;
        this.childNamespace = ns;
        this.multiplicity = multi;
        this.generateStatementList();
    }  
    
    private ArrayList<PyStatement> generateStatementList()
    {
        /* initialize variables */
        this.getterAndSetter = new ArrayList<PyFunctionDefinition>();
        this.imports =  new ArrayList<PyImportStatement>();
        
        ArrayList<PyStatement> tmp = new ArrayList<PyStatement>();
        boolean addImport = false;
        
        
        /* prepare the assignment */
        PyName nullValue = new PyName("None");
        PyName value = new PyName("None");
        if (this.multiplicity == multiplicityT.LIST_NO_MIN)
        {
            value = new PyName("[]");
        } else if (this.multiplicity == multiplicityT.LIST_MIN_ONE) {
            value = new PyName("[ " + this.child + "() ]");
            addImport = true;
        } else if (this.multiplicity == multiplicityT.ONE) {
            value = new PyName(this.child + "()");
            addImport = true;
        } 
        
        /* add import if needed */
        PyImportStatement importStmnt = new PyImportStatement();
        if (addImport)
        {
            importStmnt.setValue(this.childNamespace);
            //this.addImportStatement(stmnt);
        }
        
        /* prepare assignment */
        PyAssignmentExpression privateAssignment = new PyAssignmentExpression();
        PyAssignmentExpression privateInstAssignment = new PyAssignmentExpression();
        PyName privateVar = new PyName(this.privateName());
        PyName privateInstanceVar = new PyName("self." + this.privateName());
        
        privateAssignment.addTarget(privateVar);
        privateInstAssignment.addTarget(privateInstanceVar);
        privateAssignment.setValue(nullValue);
        privateInstAssignment.setValue(value);
        
        PyArgument selfArg = new PyArgument("self");
        
        /* prepare the getter */
        PyFunctionDefinition getter = new PyFunctionDefinition(this.name);
        PyDecoratorStatement propDec = new PyDecoratorStatement();
        getter.addArgument(selfArg);
        propDec.setValue(PyDecoratorStatement.DecoratorStore.PROPERTY);
        getter.addDecorator(propDec);
        PyHelpStatement helpGetter = new PyHelpStatement();
        helpGetter.setBody("this method returns the value of the private property: " + this.privateName());
        PyReturnStatement getReturn = new PyReturnStatement();
        getReturn.setValue("self." + this.privateName());
        
        /* prepare the default value in the getter */
        PyConditionalStatement valueSetup = new PyConditionalStatement();
        PyComparisonExpression notNoneExpr =  new PyComparisonExpression();
        notNoneExpr.addOperand(privateInstanceVar);
        notNoneExpr.addOperand(nullValue);
        notNoneExpr.setOperator(PyExpression.ComparisonOperator.EQUAL);
        valueSetup.setCondition(notNoneExpr);
        if (addImport)
        {
            valueSetup.addThenStatement(importStmnt);
        }
        valueSetup.addThenStatement(privateInstAssignment);
        
        getter.addBodyStatement(helpGetter);
        getter.addBodyStatement(valueSetup);
        getter.addBodyStatement(getReturn);
        
        /* prepare setter function */
        PyFunctionDefinition setter = new PyFunctionDefinition("set" + this.name.substring(0, 1).toUpperCase() + this.name.substring(1));
        PyArgument setterArg = new PyArgument("value");
        PyHelpStatement helpSetter = new PyHelpStatement();
        PyAssignmentExpression setVal = new PyAssignmentExpression();
        PyString privateInstVariable = new PyString();
        PyString setterValueStr = new PyString();
        privateInstVariable.addValue("self." + this.privateName());
        setterValueStr.addValue(setterArg.toString());
        setVal.addTarget(privateInstVariable);
        setVal.setValue(setterValueStr);
        helpSetter.setBody("this method sets the value of the private property: " + this.privateName());
        
        setter.addArgument(selfArg);
        setter.addArgument(setterArg);
        setter.addBodyStatement(helpSetter);
        if (this.multiplicity != multiplicityT.NONE && this.multiplicity != multiplicityT.NULL_OR_ONE)
        {
            /* prepare the default value in the setter */
            valueSetup = new PyConditionalStatement();
            notNoneExpr =  new PyComparisonExpression();
            notNoneExpr.addOperand(setterValueStr);
            notNoneExpr.addOperand(nullValue);
            notNoneExpr.setOperator(PyExpression.ComparisonOperator.EQUAL);
            valueSetup.setCondition(notNoneExpr);
            
            PyAssignmentExpression setDefault = new PyAssignmentExpression();
            PyName setArg = new PyName("self.value");
            PyName getVal = new PyName("self." + this.name);
            setDefault.addTarget(setArg);
            setDefault.setValue(getVal);
            
            valueSetup.addThenStatement(setDefault);
            setter.addBodyStatement(valueSetup);
        }
        
        setter.addBodyStatement(setVal);
        
        this.getterAndSetter.add(getter);
        this.getterAndSetter.add(setter);
        
        tmp.add(privateAssignment);
        tmp.add(getter);
        tmp.add(setter);
        
        return tmp;
    }
    

    private String privateName()
    {
        return "_" + this.name;
    }
    

    public String toString()
    {
        return "[PyAssociationDefinition '" + this.name + "', " + this.name.length()
                + " statements]";
    }

    
    public String getValue()
    {
        String tmp = this.privateName();
        tmp += " " + this.name;
        tmp += " " + multiplicityToString(this.multiplicity);
        return tmp;
    }
    
    
    public ArrayList<PyImportStatement> getImports()
    {
        return this.imports;
    }
    
    
    public boolean importExists(PyImportStatement imp)
    {
        for (PyImportStatement checkImport: this.imports)
        {
            if (checkImport.getValue().contentEquals(imp.getValue()))
            {
                return true;
            }
        }
        return false;
    }
    
    public void addImportStatement(ArrayList<PyImportStatement> imports)
    {
        for (PyImportStatement eachImport: imports)
        {
            this.addImportStatement(eachImport);
        }
    }
    
    public void addImportStatement(PyImportStatement imp)
    {
        if (!this.importExists(imp))
        {
            this.imports.add(imp);
        }
    }
    
    
    @Override
    public String toCode()
    {
        return "";
    }
    
    
    public String toCode(int indent)
    {
        StringBuffer dst = new StringBuffer("\n");
        
        this.indent(dst, indent);
        dst.append("# this is the association " + this.name + "\n");
        ArrayList<PyStatement> tmp = this.generateStatementList();
        for (PyStatement eachStatement: tmp)
        {
            this.toCode(eachStatement, dst, indent);
        }
        dst.append("\n");
        this.indent(dst, indent);
        return dst.toString();
    }
    

}
