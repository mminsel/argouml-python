// $Id: PyClass.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package org.argouml.python.py;

import java.util.HashSet;
import java.util.Set;

public class PyClass extends PyElement
{
    private Set<PyClass> baseClasses = new HashSet<PyClass>();

    public PyClass(String name, PyElement parent)
    {
        super("class", name, parent);
    }

    public PyClass(String name)
    {
        this(name, null);
    }

    public void addBaseClass(PyClass b)
    {
        this.baseClasses.add(b);
    }

    public Set<PyClass> getBaseClasses()
    {
        return this.baseClasses;
    }

    public Set<PyAttribute> getClassAttributes()
    {
        Set<PyAttribute> res = new HashSet<PyAttribute>();
        for (PyElement e : getChildren()) {
            if ("attribute".equals(e.getType()))
            {
                PyAttribute a = (PyAttribute) e;
                if (a.isClassAttribute())
                {
                    res.add(a);
                }
            }
        }

        return res;
    }

    public Set<PyAttribute> getInstanceAttributes()
    {
        Set<PyAttribute> res = new HashSet<PyAttribute>();
        for (PyElement e : getChildren()) {
            if ("attribute".equals(e.getType()))
            {
                PyAttribute a = (PyAttribute) e;
                if (!a.isClassAttribute())
                {
                    res.add(a);
                }
            }
        }

        return res;
    }

    public PyFunction getConstructor()
    {
        for (PyElement e : getChildren()) {
            if ("function".equals(e.getType()) 
                    && "__init__".equals(e.getName()))
            {
                return (PyFunction) e;
            }
        }

        return null;
    }
}
