// $Id: PyAssignmentExpression.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

import java.util.ArrayList;

public class PyAssignmentExpression extends PyExpression
{
    private PyExpression.BinaryOperator modifier;
    private ArrayList<PyExpression> targets = new ArrayList<PyExpression>();
    private PyExpression value;

    public PyAssignmentExpression(PyExpression.BinaryOperator operator)
    {
        this.modifier = operator;
    }

    public PyAssignmentExpression()
    {
        this(null);
    }

    public void setModifier(PyExpression.BinaryOperator operator)
    {
        this.modifier = operator;
    }

    public PyExpression.BinaryOperator getModifier()
    {
        return this.modifier;
    }

    public void addTarget(PyExpression target)
    {
        if (target != null)
        {
            this.targets.add(target);
        }
    }

    public void addTargets(ArrayList<PyExpression> ts)
    {
        if (ts == null)
        {
            return;
        }

        for (PyExpression e : ts)
        {
            this.addTarget(e);
        }
    }

    public PyExpression getTarget(int i)
    {
        return this.targets.get(i);
    }

    public ArrayList<PyExpression> getTargets()
    {
        return this.targets;
    }

    public PyExpression getValue()
    {
        return value;
    }

    public void setValue(PyExpression value)
    {
        this.value = value;
    }
    
    @Override
    public String toString()
    {
        return this.toCode();
    }

    @Override
    public String toCode()
    {
        StringBuffer tmp = new StringBuffer();
        boolean first = true;
        for (PyExpression t : this.targets)
        {
            if (first)
            {
                first = false;
            }
            else
            {
                tmp.append(", ");
            }
            tmp.append(t.toString());   
        }

        tmp.append(" = ");

        if (this.value != null)
        {
            tmp.append(this.value.toString());
        }
        else
        {
            tmp.append("None");
        }

        return tmp.toString();
    }
}
