// $Id: PyTryStatement.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, 
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

import java.util.ArrayList;

public class PyTryStatement extends PyStatement
{
    private ArrayList<PyStatement> finalBlock = new ArrayList<PyStatement>();
    private ArrayList<PyStatement> body = new ArrayList<PyStatement>();
    private ArrayList<PyExceptClause> clauses = new ArrayList<PyExceptClause>();

    public ArrayList<PyStatement> getBody()
    {
        return body;
    }

    public void setBody(ArrayList<PyStatement> body)
    {
        this.body = body;
    }

    public void addBodyStatement(PyStatement s)
    {
        if (s != null)
        {
            this.body.add(s);
        }
    }

    public void addBodyStatements(ArrayList<PyStatement> stmts)
    {
        if (stmts == null)
        {
            return;
        }

        for (PyStatement s : stmts)
        {
            this.addBodyStatement(s);
        }
    }

    public ArrayList<PyStatement> getFinal()
    {
        return finalBlock;
    }

    public void setFinal(ArrayList<PyStatement> finalBlock)
    {
        this.finalBlock = finalBlock;
    }

    public void addFinalStatement(PyStatement s)
    {
        this.finalBlock.add(s);
    }

    public void addFinalStatements(ArrayList<PyStatement> s)
    {
        this.finalBlock.addAll(s);
    }

    public ArrayList<PyExceptClause> getClauses()
    {
        return clauses;
    }

    public void setClauses(ArrayList<PyExceptClause> clauses)
    {
        this.clauses = clauses;
    }

    public void addClause(PyExceptClause c)
    {
        this.clauses.add(c);
    }
}
