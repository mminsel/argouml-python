//$Id: PyConditionalStatement.java 93 2010-01-12 19:40:42Z linus $ 
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

import java.util.ArrayList;

public class PyConditionalStatement extends PyCompoundStatement
{
    private ArrayList<PyStatement> thenStatements = new ArrayList<PyStatement>();
    private ArrayList<PyStatement> elseStatements = new ArrayList<PyStatement>();
    private ArrayList<PyConditionalStatement> elseIfStatements = new ArrayList<PyConditionalStatement>();
    private PyExpression condition;

    public PyConditionalStatement()
    {
    }

    public ArrayList<PyStatement> getThenStatements()
    {
        return thenStatements;
    }

    public void setThenStatements(ArrayList<PyStatement> thenStatements)
    {
        this.thenStatements = thenStatements;
    }

    public void addThenStatement(PyStatement s)
    {
        if (s != null)
        {
            this.thenStatements.add(s);
        }
    }

    public void addThenStatements(ArrayList<PyStatement> stmts)
    {
        if (stmts == null)
        {
            return;
        }

        for (PyStatement s : stmts)
        {
            this.addThenStatement(s);
        }
    }

    public ArrayList<PyStatement> getElseStatements()
    {
        return elseStatements;
    }

    public void setElseStatements(ArrayList<PyStatement> elseStatements)
    {
        this.elseStatements = elseStatements;
    }

    public void addElseStatement(PyStatement s)
    {
        if (s != null)
        {
            this.elseStatements.add(s);
        }
    }

    public void addElseStatements(ArrayList<PyStatement> stmts)
    {
        if (stmts == null)
        {
            return;
        }

        for (PyStatement s : stmts)
        {
            this.addElseStatement(s);
        }
    }

    public ArrayList<PyConditionalStatement> getElseIfStatements()
    {
        return elseIfStatements;
    }

    public void setElseIfStatements(
            ArrayList<PyConditionalStatement> elseIfStatements)
    {
        this.elseIfStatements = elseIfStatements;
    }

    public void addElseIfStatement(PyConditionalStatement c)
    {
        if (c != null)
        {
            this.elseIfStatements.add(c);
        }
    }

    public void addElseIfStatements(ArrayList<PyConditionalStatement> stmts)
    {
        if (stmts == null)
        {
            return;
        }

        for (PyConditionalStatement c : stmts)
        {
            this.addElseIfStatement(c);
        }
    }

    public PyExpression getCondition()
    {
        return condition;
    }

    public void setCondition(PyExpression condition)
    {
        this.condition = condition;
    }

    public String toCode(int indent)
    {
        return this.toCode(indent, false);
    }
    public String toCode(int indent, boolean isElif)
    {
        StringBuffer dst = new StringBuffer("");
        if (!isElif)
        {
            this.indent(dst, indent);
        }
        dst.append("if ");
        dst.append(this.getCondition().toCode());
        dst.append(":\n");
        for (PyStatement eachStatement: this.getThenStatements())
        {
            if (eachStatement instanceof PyCompoundStatement)
            {
                ((PyCompoundStatement) eachStatement).toCode(eachStatement, dst, indent+1);
            } else {
                this.indent(dst, indent+1);
                dst.append(eachStatement.toString());
            }
            dst.append("\n");
        }
        for (PyConditionalStatement eachElif: this.getElseIfStatements())
        {
            this.indent(dst, indent);
            dst.append("el");
            dst.append(eachElif.toCode(indent, true));
        }
        if (this.getElseStatements().size() == 0){
            return dst.toString();
        }
        for (PyStatement eachStatement: this.getElseStatements())
        {
            if (eachStatement instanceof PyCompoundStatement)
            {
                ((PyCompoundStatement) eachStatement).toCode(eachStatement, dst, indent+1);
            } else {
                this.indent(dst, indent+1);
                dst.append(eachStatement.toString());
            }
            dst.append("\n");
        }
        
        return dst.toString();
    }
}
