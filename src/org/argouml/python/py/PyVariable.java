// $Id: PyVariable.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package org.argouml.python.py;

public class PyVariable extends PyElement
{
    private String vartype;
    private String initialValue;

    public PyVariable(String name, String vartype, String initialValue, 
            PyElement parent) 
    {
        super("variable", name, parent);
        this.vartype = vartype;
        this.initialValue = initialValue;
    }

    public PyVariable(String name, String vartype, PyElement parent)
    {
        this(name, vartype, null, parent);
    }

    public PyVariable(String name, String vartype)
    {
        this(name, vartype, null, null);
    }

    public PyVariable(String name, PyElement parent)
    {
        this(name, null, null, parent);
    }

    public PyVariable(String name)
    {
        this(name, null, null, null);
    }

    public boolean hasInitialValue()
    {
        return this.initialValue != null;
    }

    public String getInitialValue()
    {
        return this.initialValue;
    }

    public void setInitialValue(String v)
    {
        this.initialValue = v;
    }

    public boolean hasVariableType()
    {
        return this.vartype != null;
    }

    public String getVariableType()
    {
        return this.vartype;
    }

    public void setVariableType(String v)
    {
        this.vartype = v;
    }
}
