// $Id: PyCallExpression.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.argouml.python.py;

import java.util.ArrayList;

public class PyCallExpression extends PyExpression
{
    private ArrayList<PyExpression> argumentList = new ArrayList<PyExpression>();
    private PyExpression expression;

    public ArrayList<PyExpression> getArgumentList()
    {
        return argumentList;
    }

    public void addArgument(PyExpression e)
    {
        if (e != null)
        {
            this.argumentList.add(e);
        }
    }

    public void addArguments(ArrayList<PyExpression> args)
    {
        if (args == null)
        {
            return;
        }

        for (PyExpression e : args)
        {
            this.addArgument(e);
        }
    }

    public PyExpression getExpression()
    {
        return expression;
    }

    public void setExpression(PyExpression expression)
    {
        this.expression = expression;
    }

    public String toCode()
    {
        StringBuffer tmp = new StringBuffer();
        tmp.append(this.expression);
        tmp.append("(");

        boolean first = true;

        for (PyExpression e : this.argumentList)
        {
            if (first)
            {
                first = false;
            }
            else
            {
                tmp.append(", ");
            }
            tmp.append(e.toString());
        }
        tmp.append(")");

        return tmp.toString();
    }
}
