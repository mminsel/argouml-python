// $Id: PyFunction.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package org.argouml.python.py;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class PyFunction extends PyElement
{
    private boolean isMethod;

    // we distinguish 'not implemented' and 'abstract' in the way,
    // that an abstract function will raise an NotImplementedError and
    // a non-abstract and not implemented method will pass.
    private boolean isImplemented;
    private boolean isAbstract;
    private boolean isStatic;

    private boolean hasVariableArguments;
    private boolean hasVariableKeywordArguments;
    private List<String> argumentNames;

    public PyFunction(String name, PyElement parent) 
    {
        super("function", name, parent);
        this.isMethod = false;
        this.hasVariableArguments = false;
        this.hasVariableKeywordArguments = false;
        this.argumentNames = new ArrayList<String>();
        this.isImplemented = false;
        this.isAbstract = false;
        this.isStatic = false;
    }

    public PyFunction(String name) 
    {
        this(name, null);
    }

    public List<String> getArgumentNames()
    {
        return this.argumentNames;
    }

    public boolean isStaticMethod()
    {
        return this.isMethod && this.isStatic;
    }

    public void isStaticMethod(boolean b)
    {
        this.isStatic = b;
        if (b)
        {
            this.isMethod = true;
        }
    }

    public boolean isMethod()
    {
        return this.isMethod;
    }

    public void isMethod(boolean b)
    {
        this.isMethod = b;
        if (!b)
        {
            this.isStatic = false;
        }
    }

    public boolean isImplemented()
    {
        return this.isImplemented;
    }

    public boolean isAbstract()
    {
        return this.isAbstract;
    }

    public void isAbstract(boolean b)
    {
        if (b && !this.isImplemented)
        {
            this.isAbstract = b;
        }
    }

    public void add(PyVariable v, boolean isArgument)
    {
        if ("self".equals(v.getName()))
        {
            this.isMethod = true;
            return;
        }

        this.add(v);
        if (isArgument)
        {
            this.argumentNames.add(v.getName());
        }
    }

    public void add(PyBlock b)
    {
        super.add(b);
        this.isImplemented = true;
        this.isAbstract = false;
    }

    public boolean hasVariableArguments()
    {
        return this.hasVariableArguments;
    }

    public void hasVariableArguments(boolean b)
    {
        this.hasVariableArguments = b;
    }

    public boolean hasVariableKeywordArguments()
    {
        return this.hasVariableKeywordArguments;
    }

    public void hasVariableKeywordArguments(boolean b)
    {
        this.hasVariableKeywordArguments = b;
    }

    public Set<PyVariable> getArguments()
    {
        Set<PyVariable> res = new HashSet<PyVariable>();
        for (PyElement e : getChildren()) {
            if ("variable".equals(e.getType()))
            {
                PyVariable v = (PyVariable) e;
                res.add(v);
            }
        }

        return res;
    }
}
