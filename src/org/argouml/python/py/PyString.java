// $Id: PyString.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.argouml.python.py;

import java.util.ArrayList;

public class PyString extends PyAtom
{
    private ArrayList<String> values = new ArrayList<String>();

    public PyString()
    {
    }

    public void addValue(String s)
    {
        if (s != null && !"".equals(s))
        {
            this.values.add(s);
        }
    }

    public void addValues(ArrayList<String> values)
    {
        if (values == null)
        {
            return;
        }

        for (String v : values)
        {
            this.addValue(v);
        }
    }

    public ArrayList<String> getValues()
    {
        return this.values;
    }

    public String toCode()
    {
        StringBuffer tmp = new StringBuffer();

        boolean first = true;
        for (String v : this.values)
        {
            if (first)
            {
                first = false;
            }
            else
            {
                tmp.append(" ");
            }
            tmp.append(v);
        }

        return tmp.toString();
    }
}
