// $Id: PyForLoop.java 93 2010-01-12 19:40:42Z linus $ 
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

import java.util.ArrayList;

public class PyForLoop extends PyCompoundStatement
{
    private ArrayList<PyStatement> body = new ArrayList<PyStatement>();
    private ArrayList<PyStatement> elseBody = new ArrayList<PyStatement>();
    private PyExpression target;
    private PyExpression iterable;

    public PyForLoop()
    {
    }

    public ArrayList<PyStatement> getBody()
    {
        return body;
    }

    public void setBody(ArrayList<PyStatement> body)
    {
        this.body = body;
    }

    public void addBodyStatement(PyStatement s)
    {
        if (s != null)
        {
            this.body.add(s);
        }
    }

    public void addBodyStatements(ArrayList<PyStatement> stmts)
    {
        if (stmts == null)
        {
            return;
        }

        for (PyStatement s : stmts)
        {
            this.addBodyStatement(s);
        }
    }

    public ArrayList<PyStatement> getElseBody()
    {
        return this.elseBody;
    }

    public void setElseBody(ArrayList<PyStatement> elseBody)
    {
        this.elseBody = elseBody;
    }

    public void addElseBodyStatement(PyStatement s)
    {
        if (s != null)
        {
            this.elseBody.add(s);
        }
    }

    public void addElseBodyStatements(ArrayList<PyStatement> stmts)
    {
        if (stmts == null)
        {
            return;
        }

        for (PyStatement s : stmts)
        {
            this.addElseBodyStatement(s);
        }
    }

    public PyExpression getTarget()
    {
        return target;
    }

    public void setTarget(PyExpression target)
    {
        this.target = target;
    }

    public PyExpression getIterable()
    {
        return iterable;
    }

    public void setIterable(PyExpression iterable)
    {
        this.iterable = iterable;
    }
}
