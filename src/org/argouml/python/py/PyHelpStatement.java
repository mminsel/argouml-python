// $Id: PyReturnStatement.java 93 2010-01-12 19:40:42Z linus $ 
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

import java.util.ArrayList;

public class PyHelpStatement extends PyStatement
{
    private String body = "";
    private String author = "";
    private String version = "";
    private String since = "";
    private String see = "";
    private boolean deprecated = false;
    
    public PyHelpStatement()
    {
        super();
    }
    public PyHelpStatement(String body)
    {
        super();
        this.body = body;
    }
    public PyHelpStatement(String body, String author)
    {
        this(body);
        this.author = author;
    }
    public PyHelpStatement(String body, String author, String version)
    {
        this(body, author);
        this.version = version;
    }
    public PyHelpStatement(String body, String author, String version, String since)
    {
        this(body, author, version);
        this.since = since;
    }
    public PyHelpStatement(String body, String author, String version, String since, String see)
    {
        this(body, author, version, since);
        this.see = see;
    }
    public PyHelpStatement(String body, String author, String version, String since, String see, boolean deprecated)
    {
        this(body, author, version, since, see);
        this.deprecated = deprecated;
    }
    
    
    public String getValue()
    {
        return this.toCode();
    }
    
    public void setBody(String value)
    {
        this.body = value;
    }
    public void setAuthor(String value)
    {
        this.author = value;
    }
    public void setVersion(String value)
    {
        this.version = value;
    }
    public void setSince(String value)
    {
        this.since = value;
    }
    public void setSee(String value)
    {
        this.see = value;
    }
    public void setDeprecated(boolean value)
    {
        this.deprecated = value;
    }
    
    public boolean isEmpty()
    {
        if (this.body.isEmpty() && this.author.isEmpty() &&
                this.version.isEmpty() && this.since.isEmpty() &&
                this.see.isEmpty() && !this.deprecated)
        {
            return true;
        }
        return false;
    }
    
    
    @Override
    public String toCode()
    {
        return toCode(1);
    }
    
   
    public String toCode(int indent)
    {
        if (this.isEmpty())
        {
            return "";
        }
        
        String newline = "\n";
        for (int i=0; i<indent; i++)
        {
            newline += "\t";
        }
        
        String str = "''' ";
        if (this.deprecated)
        {
            str += "WARNING: this method is deprecated and should NOT be used!";
        }
        if (!this.body.isEmpty())
        {
            if (this.deprecated)
            {
                str += newline+newline+"\t";
            }
            str += "@brief: " + this.body.replace("\r", "").replace("\n", newline+"\t\t");
        }
        if (!this.author.isEmpty())
        {
            str += newline+"\t@author: " + this.author.replace("\r", "").replace("\n", newline+"\t\t");
        }
        if (!this.version.isEmpty())
        {
            str += newline+"\t@version: " + this.version.replace("\r", "").replace("\n", newline+"\t\t");
        }
        if (!this.since.isEmpty())
        {
            str += newline+"\t@since: " + this.since.replace("\r", "").replace("\n", newline+"\t\t");
        }
        if (!this.see.isEmpty())
        {
            str += newline+"\t@see: " + this.see.replace("\r", "").replace("\n", newline+"\t\t");
        }
        str += newline+"'''"+newline;
        return str;
    }
}
