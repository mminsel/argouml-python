// $Id: PyClassDefinition.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

import java.util.ArrayList;

public class PyClassDefinition extends PyCompoundStatement
{
    private String name;
    private ArrayList<PyImportStatement> imports = new ArrayList<PyImportStatement>();
    private ArrayList<PyAssociationDefinition> associations = new ArrayList<PyAssociationDefinition>();
    private ArrayList<PyStatement> body = new ArrayList<PyStatement>();
    private PyExpression bases;

    public PyClassDefinition(String name)
    {
        this.name = name;
    }

    public PyClassDefinition()
    {
        this(null);
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public ArrayList<PyStatement> getBody()
    {
        ArrayList<PyStatement> out = new ArrayList<PyStatement>();
        out.addAll(this.body);
        return out;
    }

    public void setBody(ArrayList<PyStatement> body)
    {
        this.body = body;
    }

    public boolean importExists(PyImportStatement imp)
    {
        for (PyImportStatement checkImport: this.imports)
        {
            if (checkImport.getValue().contentEquals(imp.getValue()))
            {
                return true;
            }
        }
        return false;
    }
    
    public void addImportStatement(ArrayList<PyImportStatement> imports)
    {
        for (PyImportStatement eachImport: imports)
        {
            this.addImportStatement(eachImport);
        }
    }
    
    public void addImportStatement(PyImportStatement imp)
    {
        if (!this.importExists(imp))
        {
            this.imports.add(imp);
        }
    }
    
    
    public boolean associationExists(PyAssociationDefinition assoc)
    {
        for (PyAssociationDefinition checkAssoc: this.associations)
        {
            if (checkAssoc.getValue().contentEquals(assoc.getValue()))
            {
                return true;
            }
        }
        return false;
    }
    
    public void addAssociationStatement(ArrayList<PyAssociationDefinition> associationList)
    {
        for (PyAssociationDefinition eachAssociation: associationList)
        {
            this.addAssociationStatement(eachAssociation);
        }
    }
    
    public void addAssociationStatement(PyAssociationDefinition assoc)
    {
        if (!this.associationExists(assoc))
        {
            this.associations.add(assoc);
        }
    }
    
    
    public void addBodyStatement(PyStatement s)
    {
        if (s != null)
        {
            this.body.add(s);
        }
    }

    public void addBodyStatements(ArrayList<PyStatement> stmts)
    {
        if (stmts == null)
        {
            return;
        }

        for (PyStatement s : stmts)
        {
            this.addBodyStatement(s);
        }
    }

    public PyExpression getBases()
    {
        return bases;
    }

    public void setBases(PyExpression bases)
    {
        this.bases = bases;
    }

    public ArrayList<PyImportStatement> getImportStatements()
    {
        return imports;
    }
    
    public ArrayList<PyFunctionDefinition> getFunctionDefinitions()
    {
        ArrayList<PyFunctionDefinition> res = 
            new ArrayList<PyFunctionDefinition>();
        for (PyStatement s : this.body)
        {
            if (s instanceof PyFunctionDefinition)
            {
                res.add((PyFunctionDefinition) s);
            }
        }
        return res;
    }

    @Override
    public String toString()
    {
        return "[PyClassDefinition '" + this.name + "', " + this.body.size()
                + " statements]";
    }

    public ArrayList<PyAssignmentExpression> getAssignmentExpressions()
    {
        ArrayList<PyAssignmentExpression> res = 
            new ArrayList<PyAssignmentExpression>();
        for (PyStatement s : this.body)
        {
            if (s instanceof PyAssignmentExpression)
            {
                res.add((PyAssignmentExpression) s);
            }
        }
        return res;
    }
    
    
    public String toCode(int indentLevel)
    {
        StringBuffer dst = new StringBuffer("");
        this.indent(dst, indentLevel);
        
        dst.append("# imports..\n");
        ArrayList<PyImportStatement> imports = this.getImportStatements();
        for (PyAssociationDefinition asso: this.associations)
        {
            for (PyImportStatement eachImport: asso.getImports())
            {
                if (!this.importExists(eachImport))
                {
                    imports.add(eachImport);
                }
            }
        }
        for (PyImportStatement eachImport: imports)
        {
            dst.append(eachImport.toCode());
            dst.append("\n");
            this.indent(dst, indentLevel);
        }
        
        dst.append("\n");
        this.indent(dst, indentLevel);
        
        dst.append("class ");
        dst.append(this.getName());
        dst.append("(");

        PyExpression bases = this.getBases();

        if (bases == null)
        {
            dst.append("object");
        }
        else
        {
            dst.append(bases.toString());
        }

        dst.append("):\n");

        for (PyAssociationDefinition asso: this.associations)
        {
            dst.append(asso.toCode(indentLevel+1));
        }
        
        for (PyStatement s : this.getBody())
        {
            this.toCode(s, dst, indentLevel + 1);
        }
        
        dst.append("\n");
        
        return dst.toString();
    }
}
