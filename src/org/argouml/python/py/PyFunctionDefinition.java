// $Id: PyFunctionDefinition.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

import java.util.ArrayList;

public class PyFunctionDefinition extends PyCompoundStatement
{
    private String name;
    private ArrayList<PyDecoratorStatement> decorators = new ArrayList<PyDecoratorStatement>();
    private ArrayList<PyStatement> body = new ArrayList<PyStatement>();
    private ArrayList<PyArgument> arguments = new ArrayList<PyArgument>();

    public PyFunctionDefinition(String name)
    {
        this.name = name;
    }

    public PyFunctionDefinition()
    {
        this(null);
    }

    public boolean isEmpty()
    {
        if (this.body == null)
        {
            return true;
        }
        else if (this.body.size() == 0)
        {
            return true;
        }
        else if (this.body.size() == 1)
        {
            PyStatement e = this.body.get(0);
            if (e instanceof PyPassStatement)
            {
                return true;
            }
        }

        return false;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return this.name;
    }

    public ArrayList<PyArgument> getArguments()
    {
        return arguments;
    }

    public void setArguments(ArrayList<PyArgument> arguments)
    {
        this.arguments = arguments;
    }

    public void addArgument(PyArgument a)
    {
        if (a != null)
        {
            this.arguments.add(a);
        }
    }
    
    public void addDecorator(PyDecoratorStatement dec)
    {
        decorators.add(dec);
    }

    public void addArguments(ArrayList<PyArgument> args)
    {
        if (args == null)
        {
            return;
        }

        for (PyArgument arg : args)
        {
            this.addArgument(arg);
        }
    }
    
    public ArrayList<PyDecoratorStatement> getDecorators()
    {
        return decorators;
    }
    
    public ArrayList<PyStatement> getBody()
    {
        return body;
    }

    public void setBody(ArrayList<PyStatement> body)
    {
        this.body = body;
    }

    public void addBodyStatement(PyStatement s)
    {
        if (s != null)
        {
            this.body.add(s);
        }
    }

    public void addBodyStatements(ArrayList<PyStatement> stmts)
    {
        if (stmts == null)
        {
            return;
        }

        for (PyStatement s : stmts)
        {
            this.addBodyStatement(s);
        }
    }

    public String toString()
    {
        return "[PyFunctionDefinition '" + this.name + "', " + this.body.size()
                + " statements]";
    }

    public ArrayList<PyAssignmentExpression> getAssigmentExpressions()
    {
        ArrayList<PyAssignmentExpression> result = new ArrayList<PyAssignmentExpression>();

        for (PyStatement s : this.body)
        {
            if (s instanceof PyAssignmentExpression)
            {
                result.add((PyAssignmentExpression) s);
            }
        }
        
        return result;
    }
    
    public String toCode(int indentLevel)
    {
        StringBuffer dst = new StringBuffer("");
        this.indent(dst, indentLevel);
        
        for (PyDecoratorStatement pds: this.getDecorators())
        {
            dst.append(pds.toCode());
            dst.append("\n");
            this.indent(dst, indentLevel);
        }
        
        dst.append("def ");
        dst.append(this.getName());
        dst.append("(");

        boolean first = true;
        for (PyArgument arg : this.getArguments())
        {
            if (first)
            {
                first = false;
            }
            else
            {
                dst.append(", ");
            }

            dst.append(arg.toString());
        }

        dst.append("):\n");

        int tmpIdnt = indentLevel + 1;
        for (PyStatement s : this.getBody())
        {
            this.toCode(s, dst, tmpIdnt);
        }
        
        if (this.isEmpty())
        {
            this.indent(dst, indentLevel + 1);
            dst.append("pass\n");
        }
        
        dst.append("\n");
        return dst.toString();
    }

}
