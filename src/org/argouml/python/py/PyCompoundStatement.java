/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

public class PyCompoundStatement extends PyStatement
{
    protected void indent(StringBuffer dst, int indentLevel)
    {
        for (int i = 0; i < indentLevel; i++)
        {
            dst.append("\t");
        }
    }
    
    protected void toCode(PyStatement s, StringBuffer dst, int indentLevel)
    {
        if (s instanceof PyClassDefinition)
        {
            this.toCode((PyClassDefinition) s, dst, indentLevel);
        }
        else if (s instanceof PyFunctionDefinition)
        {
            dst.append(((PyFunctionDefinition)s).toCode(indentLevel));
        }
        else if (s instanceof PyConditionalStatement)
        {
            dst.append(((PyConditionalStatement)s).toCode(indentLevel));
        }
        else if (s instanceof PyHelpStatement)
        {
            this.indent(dst, indentLevel);
            dst.append(((PyHelpStatement)s).toCode(indentLevel));
            dst.append("\n");
        }
        else if (s instanceof PyExpression)
        {
            PyExpression e = (PyExpression) s;
            this.indent(dst, indentLevel);
            dst.append(e.toString());
            dst.append("\n");
        }

        else
        {
            //this.log.error("can't handle " + s);

        }
    }
}
