// $Id: PyReturnStatement.java 93 2010-01-12 19:40:42Z linus $ 
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

import java.util.ArrayList;

public class PyDecoratorStatement extends PyFlowStatement
{
    private String value;
    
    public enum DecoratorStore {
        STATICMETHOD, CLASSMETHOD, PROPERTY
    }
    
    public static String decoratorStr(DecoratorStore number)
    {
        switch (number)
        {
            case STATICMETHOD:
                return "@staticmethod";
            case CLASSMETHOD:
                return "@classmethod";
            case PROPERTY:
                return "@property";
        }
        return "";
    }
    
    public PyDecoratorStatement()
    {
        super();
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String val)
    {
        if (val.startsWith("@"))
        {
            this.value = val;
        } else {
            this.value = "@" + val;
        }
    }
    
    public void setValue(DecoratorStore decorator)
    {
        
        this.value = decoratorStr(decorator);
    }

    @Override
    public String toCode()
    {
        if (this.value == null)
        {
            return "";
        }
        else
        {
            return this.value;
        }
    }
}
