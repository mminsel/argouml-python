// $Id: PyNumber.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, 
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

public class PyNumber extends PyAtom
{
    public static final int INTEGER = 0;
    public static final int LONG = 1;
    public static final int FLOAT = 2;
    public static final int COMPLEX = 3;

    private int type;
    private String value;

    public PyNumber(int type)
    {
        this.type = type;
        this.value = "";
    }

    public PyNumber(int type, String value)
    {
        this(type);
        this.setValue(value);
    }

    public int getType()
    {
        return this.type;
    }

    public void setValue(String value)
    {
        if (value == null)
        {
            this.value = "";
        }
        else
        {
            this.value = value;
        }
    }

    public String getValue()
    {
        return this.value;
    }

    public String toCode()
    {
        return this.value;
    }
}
