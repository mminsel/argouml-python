// $Id: PyCollection.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.argouml.python.py;

import java.util.ArrayList;

public class PyCollection extends PyAtom
{
    public static final int LIST = 0;
    public static final int TUPLE = 0;
    public static final int DICT = 0;

    private ArrayList<PyExpression> elements = new ArrayList<PyExpression>();
    private int type;

    public PyCollection(int type)
    {
        this.type = type;
    }

    public int getType()
    {
        return this.type;
    }

    public ArrayList<PyExpression> getElements()
    {
        return elements;
    }

    public void addElement(PyExpression e)
    {
        if (e != null)
        {
            this.elements.add(e);
        }
    }

    public void addElements(ArrayList<PyExpression> elements)
    {
        if (elements == null)
        {
            return;
        }

        for (PyExpression e : elements)
        {
            this.addElement(e);
        }
    }
}