// $Id: CompilationUnit2Code.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.argouml.python.transform;

import org.apache.log4j.Logger;
import org.argouml.python.py.PyArgument;
import org.argouml.python.py.PyClassDefinition;
import org.argouml.python.py.PyCompilationUnit;
import org.argouml.python.py.PyDecoratorStatement;
import org.argouml.python.py.PyExpression;
import org.argouml.python.py.PyFunctionDefinition;
import org.argouml.python.py.PyImportStatement;
import org.argouml.python.py.PyStatement;
import org.argouml.python.py.PyHelpStatement;
import java.util.ArrayList;

public class CompilationUnit2Code
{
    private Logger log;

    public CompilationUnit2Code()
    {
        this.log = Logger.getLogger(this.getClass());
    }

    public String toCode(PyCompilationUnit cu)
    {
        StringBuffer tmp = new StringBuffer();

        for (PyStatement s : cu.getStatements())
        {
            this.toCode(s, tmp, 0);
        }
        return tmp.toString();
    }

    private void toCode(PyStatement s, StringBuffer dst, int indentLevel)
    {
        if (s instanceof PyClassDefinition)
        {
            dst.append(((PyClassDefinition) s).toCode(indentLevel));
        }
        else if (s instanceof PyFunctionDefinition)
        {
            dst.append(((PyFunctionDefinition)s).toCode(indentLevel));
        }
        else if (s instanceof PyHelpStatement)
        {
            this.indent(dst, indentLevel);
            dst.append(((PyHelpStatement)s).toCode(indentLevel));
            dst.append("\n");
        }
        else if (s instanceof PyExpression)
        {
            PyExpression e = (PyExpression) s;
            this.indent(dst, indentLevel);
            dst.append(e.toString());
            dst.append("\n");
        }

        else
        {
            this.log.error("can't handle " + s);

        }
    }

    private void toCode(PyClassDefinition cd, StringBuffer dst, int indentLevel)
    {
        dst.append(cd.toCode(indentLevel));
    }

    private void toCode(PyFunctionDefinition fd, StringBuffer dst,
            int indentLevel)
    {
        dst.append(fd.toCode(indentLevel));
    }

    private void indent(StringBuffer dst, int indentLevel)
    {
        for (int i = 0; i < indentLevel; i++)
        {
            dst.append("\t");
        }
    }
}
