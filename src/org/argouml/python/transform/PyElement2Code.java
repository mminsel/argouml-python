// $Id: PyElement2Code.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package org.argouml.python.transform;

import java.util.Iterator;

import org.apache.log4j.Logger;

import org.argouml.python.py.PyElement;
import org.argouml.python.py.PyModule;
import org.argouml.python.py.PyClass;
import org.argouml.python.py.PyFunction;
import org.argouml.python.py.PyAttribute;
import org.argouml.python.py.PyVariable;
import org.argouml.python.py.PyBlock;

public class PyElement2Code
{
    private Logger log;

    public PyElement2Code()
    {
        this.log = Logger.getLogger(this.getClass());
    }

    public String toCode(PyElement pe)
    {
        String type = pe.getType();
        if ("module".equals(type))
        {
            return this.toCode((PyModule) pe);
        }
        else if ("class".equals(type))
        {
            return this.toCode((PyClass) pe);
        }
        else if ("function".equals(type))
        {
            return this.toCode((PyFunction) pe);
        }
        else if ("attribute".equals(type))
        {
            return this.toCode((PyAttribute) pe);
        }
        else if ("variable".equals(type))
        {
            return this.toCode((PyVariable) pe);
        }
        else if ("block".equals(type))
        {
            PyBlock pb = (PyBlock) pe;
            return pb.getCode();
        }
        else
        {
            this.log.error("can't handle type '" + type + "'");
            return null;
        }
    }

    public String toCode(PyModule pm)
    {
        // hm ... this is more a TODO
        StringBuffer res = new StringBuffer();
        StringBuffer all = new StringBuffer();
        for (Iterator<PyElement> it = pm.getChildren().iterator(); 
                it.hasNext();) {
            PyElement e = it.next();
            if ("class".equals(e.getType()))
            {
                PyClass c = (PyClass) e;
                res.append("from ");
                res.append(c.getName().toLowerCase());
                res.append(" import ");
                res.append(c.getName());
                res.append("\n");
                all.append(c.getName());

                if (it.hasNext() || pm.getChildren().size() == 1)
                {
                    all.append(", ");
                }
            }

        }
        
        res.append("__all__ = [");
        res.append(all.toString());
        res.append("]\n");

        return res.toString();
    }

    public String toCode(PyClass pc)
    {
        StringBuffer res = new StringBuffer();

        res.append("class ");
        res.append(pc.getName());
        res.append("(");
        if (pc.getBaseClasses().size() == 0)
        {
            res.append("object");
        }
        else
        {
            for (Iterator<PyClass> it = pc.getBaseClasses().iterator(); 
                    it.hasNext();)
            {
                PyClass b = it.next();
                res.append(b.getName());
                if (it.hasNext())
                {
                    res.append(", ");
                }
            }
        }
        res.append("):\n");

        // docstring
        String docstring = pc.getDocstring();
        if (docstring != null && docstring != "")
        {
            res.append("\t\"\"\"\n");
            String b[] = docstring.split("\n", 0);
            for (int i = 0; i < b.length; i++)
            {
                res.append("\t\t" + b[i] + "\n");
            }
            res.append("\t\"\"\"\n");
        }

        boolean emptyClass = true;
        // code blocks for class
        for (PyElement e : pc.getChildren()) {
            if (e.getType().equals("block"))
            {
                // write raw block
                PyBlock b = (PyBlock) e;
                String block = b.getCode();
                res.append(block + "\n");
                emptyClass = false;
            }
        }

        // methods, etc. TODO: etc :)
        for (PyElement e : pc.getChildren()) {
            if (e.getType().equals("function"))
            {
                PyFunction f = (PyFunction) e;
        
                String fString = this.toCode(f);
                String b[] = fString.split("\n", 0);
                for (int i = 0; i < b.length; i++)
                {
                    res.append("\t");
                    res.append(b[i]);
                    res.append("\n");
                }

                res.append("\n");
                emptyClass = false;
            }
        }
        
        if (emptyClass)
        {
            res.append("\tpass");
        }

        return res.toString();
    }

    public String toCode(PyFunction pf)
    {
        StringBuffer res = new StringBuffer();

        // function header
        res.append("def " + pf.getName() + "(");

        boolean needComma = false;

        if (pf.isMethod())
        {
            if (pf.isStaticMethod())
            {
                res.append("cls");
            }
            else
            {
                res.append("self");
            }
            needComma = true;
        }


        for (PyElement e : pf.getChildren()) {
            if (e.getType().equals("variable"))
            {
                PyVariable pv = (PyVariable) e;
                if (pf.getArgumentNames().contains(pv.getName())
                        || pv.hasInitialValue())
                {
                    if (needComma)
                    {
                        res.append(", ");
                    }

                    String vstr = this.toCode(pv);
                    res.append(vstr);
                }
            }
        }

        if (pf.hasVariableArguments())
        {
            res.append(", *args");
        }

        if (pf.hasVariableKeywordArguments())
        {
            res.append(", **kwargs");
        }

        res.append("):\n");

        // docstring
        if (pf.hasDocstring())
        {
            res.append("\t\"\"\"\n");
            String b[] = pf.getDocstring().split("\n", 0);
            for (int i = 0; i < b.length; i++)
            {
                res.append("\t\t" + b[i] + "\n");
            }
            res.append("\t\"\"\"\n");
        }

        for (PyElement e : pf.getChildren()) {
            if (e.getType().equals("block"))
            {
                PyBlock pb = (PyBlock) e;
                String block = pb.getCode();
                res.append(block);
            }
        }
        if (!pf.isImplemented())
        {
            if (pf.isAbstract())
            {
                res.append("\traise NotImplementedError('");
                res.append(pf.getName());
                res.append("() is not implemented.')\n");
            }
            else
            {
                res.append("\tpass\n");
            }
        }

        if (pf.isStaticMethod())
        {
            res.append(pf.getName());
            res.append(" = staticmethod(");
            res.append(pf.getName());
            res.append(")\n\n");
        }

        return res.toString();
    }

    public String toCode(PyVariable pv)
    {
        if (pv.hasInitialValue())
        {
            return pv.getName() + " = " + pv.getInitialValue();
        }
        else
        {
            return pv.getName();
        }
    }

    public String toCode(PyAttribute pa)
    {
        StringBuffer res = new StringBuffer();

        if (!pa.isClassAttribute())
        {
            res.append("self.");
        }

        if (pa.isPrivate())
        {
            res.append("__");
        }
        else if (pa.isProtected())
        {
            res.append("_");
        }

        res.append(pa.getName()); 

        if (pa.hasInitialValue())
        {
            if ("string".equals(pa.getVariableType()))
            {
                // TODO: quoting
                res.append(" = '");
                res.append(pa.getInitialValue());
                res.append("'");
            }
            else
            {
                res.append(" = ");
                res.append(pa.getInitialValue());
            }
        }
        else
        {
            if ("dict".equals(pa.getVariableType()))
            {
                res.append(" = {}");
            }
            else if ("list".equals(pa.getVariableType()))
            {
                res.append(" = []");
            }
            else
            {
                res.append(" = None");
            }
        }

        return res.toString();
    }
}
