// $Id: CompilationUnit2Model.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, 
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.argouml.python.transform;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.argouml.kernel.Project;
import org.argouml.kernel.ProjectManager;
import org.argouml.model.CoreFactory;
import org.argouml.model.CoreHelper;
import org.argouml.model.DataTypesFactory;
import org.argouml.model.DataTypesHelper;
import org.argouml.model.Facade;
import org.argouml.model.Model;
import org.argouml.model.ModelManagementFactory;
import org.argouml.model.ModelManagementHelper;
import org.argouml.model.StateMachinesHelper;
import org.argouml.model.VisibilityKind;
import org.argouml.python.py.PyArgument;
import org.argouml.python.py.PyAssignmentExpression;
import org.argouml.python.py.PyAtom;
import org.argouml.python.py.PyCallExpression;
import org.argouml.python.py.PyClassDefinition;
import org.argouml.python.py.PyCollection;
import org.argouml.python.py.PyCompilationUnit;
import org.argouml.python.py.PyExpression;
import org.argouml.python.py.PyFunctionDefinition;
import org.argouml.python.py.PyName;
import org.argouml.python.py.PyNumber;
import org.argouml.python.py.PyStatement;
import org.argouml.uml.StereotypeUtility;

public class CompilationUnit2Model
{
    private Logger log;
    private CoreFactory coreFactory;
    private CoreHelper coreHelper;
    private ModelManagementFactory modelManagementFactory;
    private ModelManagementHelper modelManagementHelper;
    private DataTypesFactory dataTypesFactory;
    private DataTypesHelper dataTypesHelper;
    private Facade facade;

    // types to use
    private Object intType;
    private Object longType;
    private Object stringType;
    private Object floatType;
    private Object boolType;

    // objects to return after transformation
    private ArrayList<Object> createdObjects = new ArrayList<Object>();

    // save created operations here
    private HashMap<String, Object> createdMethods = new HashMap<String, Object>();

    private static Set<String> vartypes = new HashSet<String>();

    public CompilationUnit2Model()
    {
        this.log = Logger.getLogger(this.getClass());

        this.coreFactory = Model.getCoreFactory();
        this.coreHelper = Model.getCoreHelper();
        this.modelManagementFactory = Model.getModelManagementFactory();
        this.modelManagementHelper = Model.getModelManagementHelper();
        this.dataTypesFactory = Model.getDataTypesFactory();
        this.dataTypesHelper = Model.getDataTypesHelper();
        this.facade = Model.getFacade();

        // dunno if these are still needed :\
        CompilationUnit2Model.vartypes.add("void");
        CompilationUnit2Model.vartypes.add("boolean");
        CompilationUnit2Model.vartypes.add("int");
        CompilationUnit2Model.vartypes.add("integer");
        CompilationUnit2Model.vartypes.add("long");
        CompilationUnit2Model.vartypes.add("float");
        CompilationUnit2Model.vartypes.add("double");
        CompilationUnit2Model.vartypes.add("string");

        // uml's builtin datatypes
        Project p = ProjectManager.getManager().getCurrentProject();
        this.intType = p.findType("Integer", false);
        this.longType = p.findType("UnlimitedInteger", false);
        this.boolType = p.findType("Boolean", false);
        this.floatType = p.findType("Float", false);
        this.stringType = p.findType("String", false);
    }

    public ArrayList<Object> getCreatedObjects()
    {
        return this.createdObjects;
    }

    public Object toModel(PyCompilationUnit cu)
    {
        ArrayList<String> tmpNamespace = this.findParentModules(cu);
        Object targetNamespace = this.getOrCreateNamespace(tmpNamespace);

        Object resultPackage;

        if ("__init__".equals(cu.getName()))
        {
            resultPackage = targetNamespace;
        }
        else
        {
            resultPackage = this.modelManagementFactory.createPackage();
            this.createdObjects.add(resultPackage);
            this.coreHelper.setName(resultPackage, cu.getModuleName());
            this.coreHelper.setNamespace(resultPackage, targetNamespace);
        }

        ArrayList<PyStatement> handled = new ArrayList<PyStatement>();

        // look for classes
        for (PyClassDefinition cd : cu.getClassDefinitions())
        {
            Object res = this.toModel(cd);
            if (res != null)
            {
                this.coreHelper.setNamespace(res, resultPackage);
            }
            handled.add(cd);
        }

        // see what else can be handled
        for (PyStatement ps : cu.getStatements())
        {
            if (handled.contains(ps))
            {
                continue;
            }

            Object res = this.toModel(ps);
            if (res != null)
            {
                this.coreHelper.setNamespace(res, resultPackage);
            }
        }

        return resultPackage;
    }

    public Object toModel(PyStatement ps)
    {
        // should be more nice :\
        if (ps instanceof PyClassDefinition)
        {
            return this.toModel((PyClassDefinition) ps);
        }
        else if (ps instanceof PyFunctionDefinition)
        {
            return this.toModel((PyFunctionDefinition) ps);
        }

        this.log.error("can't handle, ignoring: " + ps);
        return null;
    }

    private boolean handleSpecialMethods(PyAssignmentExpression ae)
    {
        PyExpression value = ae.getValue();
        ArrayList<PyExpression> targets = ae.getTargets();
        if (targets.size() > 1)
        {
            return false;
        }

        PyExpression target = targets.get(0);
        PyName targetName = null;

        if (target instanceof PyName)
        {
            targetName = (PyName) target;
        }

        if (targetName.getMemberOf() != null)
        {
            return false;
        }

        String targetString = targetName.getName();

        if (!(value instanceof PyCallExpression))
        {
            return false;
        }

        PyCallExpression callExpression = (PyCallExpression) value;
        ArrayList<PyExpression> callees = callExpression.getArgumentList();

        if (callees.size() != 1)
        {
            return false;
        }

        PyExpression callee = callees.get(0);

        if (!(callee.isAtom() && callee instanceof PyName))
        {
            return false;
        }

        String calleeName = ((PyName) callee).getName();

        if (!targetString.equals(calleeName))
        {
            return false;
        }

        PyExpression call = callExpression.getExpression();
        if (!(call.isAtom() && call instanceof PyName))
        {
            return false;
        }

        PyName callFunction = (PyName) call;
        String name = callFunction.getName();

        if ("staticmethod".equals(name))
        {
            Object operation = this.createdMethods.get(calleeName);
            if (operation != null)
            {
                this.coreHelper.setStatic(operation, true);
                return true;
            }
        }
        else if ("classmethod".equals(name))
        {
            this.log.warn("converting classmethod '" + calleeName
                    + "' to static method");
            Object operation = this.createdMethods.get(calleeName);
            if (operation != null)
            {
                this.coreHelper.setStatic(operation, true);
                return true;
            }

        }

        return true;
    }

    public Object toModel(PyClassDefinition cd)
    {
        Object resultClass = this.coreFactory.createClass();
        this.createdObjects.add(resultClass);
        this.coreHelper.setName(resultClass, cd.getName());

        ArrayList<PyStatement> handled = new ArrayList<PyStatement>();
        ArrayList<String> createdAttributes = new ArrayList<String>();
        HashMap<String, Object> createdMethods = new HashMap<String, Object>();

        // base classes
        List<String> baseNames = new ArrayList<String>();
        PyExpression baseClasses = cd.getBases();
        if (baseClasses != null)
        {
            if (baseClasses instanceof PyName)
            {
                PyName baseClass = (PyName) baseClasses;
                baseNames.add(baseClass.getName());

            }
            else if (baseClasses instanceof PyCollection)
            {
                PyCollection baseClassCollection = (PyCollection) baseClasses;
                for (PyExpression e : baseClassCollection.getElements())
                {
                    if (e instanceof PyName)
                    {
                        PyName base = (PyName) e;
                        baseNames.add(base.getName());
                    }
                    else
                    {
                        this.log.warn("ignoring base class expression: " + e);
                    }
                }
            }
            else
            {
                this.log.warn("ignoring base class expression: " + baseClasses);
            }
        }

        if (baseNames.size() == 0)
        {
            baseNames.add("object");
        }

        for (String base : baseNames)
        {
            if (base == null)
            {
                continue;
            }
            else if (base.equals("object"))
            {

            }
            else
            {
                // look for base in current file
                // look for base in imports
                // if nothing found, create the class here
                
                // this all isn't that easy, the base class could be defined 
                // - in a file parsed later
                // - in a file that won't be parsed, so namespace isn't known 
            }
        }

        // look for methods
        for (PyFunctionDefinition fd : cd.getFunctionDefinitions())
        {
            Object res = this.toModel(fd);
            if (res != null)
            {
                this.coreHelper.setOwner(res, resultClass);
                String name = this.facade.getName(res);
                createdMethods.put(name, res);
            }

            // scan for instance-attributes
            for (PyAssignmentExpression ae : fd.getAssigmentExpressions())
            {
                for (PyExpression e : ae.getTargets())
                {
                    if (e instanceof PyName)
                    {
                        PyName n = (PyName) e;
                        PyExpression memberOf = n.getMemberOf();

                        if (memberOf != null && memberOf instanceof PyName)
                        {
                            PyName tmp = (PyName) memberOf;
                            if (!"self".equals(tmp.getName()))
                            {
                                continue;
                            }

                            String attributeName = n.getName();
                            boolean exists = false;
                            for (String existing : createdAttributes)
                            {
                                if (attributeName.equals(existing))
                                {
                                    exists = true;
                                    break;
                                }
                            }

                            if (exists)
                            {
                                continue;
                            }

                            createdAttributes.add(attributeName);

                            Object attribute = this.coreFactory
                                    .createAttribute();
                            this.coreHelper.setName(attribute, attributeName);
                            this.coreHelper.setOwner(attribute, resultClass);

                            // visibility
                            VisibilityKind vk = Model.getVisibilityKind();
                            if (attributeName.startsWith("__"))
                            {
                                this.coreHelper.setVisibility(attribute, vk
                                        .getPrivate());
                            }
                            else if (attributeName.startsWith("_"))
                            {
                                this.coreHelper.setVisibility(attribute, vk
                                        .getProtected());
                            }
                            else
                            {
                                this.coreHelper.setVisibility(attribute, vk
                                        .getPublic());
                            }

                        }
                    }
                }
            }

            handled.add(fd);
        }

        // look for assignments
        // class-attributes, special methods(static, class)
        for (PyAssignmentExpression ae : cd.getAssignmentExpressions())
        {
            if (handled.contains(ae))
            {
                continue;
            }

            // put in attributes here to set initial values later
            ArrayList<Object> attributes = new ArrayList<Object>();
            if (this.handleSpecialMethods(ae))
            {
                handled.add(ae);
                continue;
            }

            for (PyExpression e : ae.getTargets())
            {
                if (e instanceof PyName)
                {
                    PyName n = (PyName) e;
                    PyExpression memberOf = n.getMemberOf();

                    // TODO: what about member != null?
                    // can't be an attribute?
                    if (memberOf == null)
                    {
                        // creating an attribute
                        String attributeName = n.getName();
                        Object attribute = this.coreFactory.createAttribute();
                        this.coreHelper.setName(attribute, attributeName);
                        this.coreHelper.setOwner(attribute, resultClass);
                        this.coreHelper.setStatic(attribute, true);

                        // visibility
                        VisibilityKind vk = Model.getVisibilityKind();
                        if (attributeName.startsWith("__"))
                        {
                            this.coreHelper.setVisibility(attribute, vk
                                    .getPrivate());
                        }
                        else if (attributeName.startsWith("_"))
                        {
                            this.coreHelper.setVisibility(attribute, vk
                                    .getProtected());
                        }
                        else
                        {
                            this.coreHelper.setVisibility(attribute, vk
                                    .getPublic());
                        }

                        attributes.add(attribute);
                    }
                }

                // try to set initial value for target(s)
                Object initialValue = null;

                // maybe the initial value gives us a type to use
                Object attributeType = null;

                PyExpression value = ae.getValue();
                if (value != null)
                {
                    if (value.isAtom())
                    {
                        PyAtom a = (PyAtom) value;
                        if (a instanceof PyNumber)
                        {
                            PyNumber n = (PyNumber) a;
                            switch (n.getType())
                            {

                                case PyNumber.INTEGER:
                                    attributeType = this.intType;
                                    break;
                                case PyNumber.LONG:
                                    attributeType = this.longType;
                                    break;
                                case PyNumber.COMPLEX:
                                    // this should definitly come from a
                                    // python-profile
                                    break;
                                case PyNumber.FLOAT:
                                    // uml has no floating point type :(
                                    break;
                            }

                            initialValue = this.dataTypesFactory
                                    .createExpression("Python", n.getValue());
                        }
                    }
                }
                for (Object attribute : attributes)
                {
                    if (initialValue != null)
                    {
                        this.coreHelper
                                .setInitialValue(attribute, initialValue);
                    }

                    if (attributeType != null)
                    {
                        this.coreHelper.setType(attribute, attributeType);
                    }
                }
            }
        }

        // see what else can be handled
        for (PyStatement ps : cd.getBody())
        {
            if (handled.contains(ps))
            {
                continue;
            }

            Object res = this.toModel(ps);
            if (res != null)
            {
                this.coreHelper.setNamespace(res, resultClass);
            }
        }

        return resultClass;
    }

    public Object toModel(PyFunctionDefinition fd)
    {
        Object resultOperation = this.coreFactory.createOperation();
        this.createdObjects.add(resultOperation);
        String name = fd.getName();
        this.coreHelper.setName(resultOperation, name);

        this.createdMethods.put(name, resultOperation);

        // stereotypes
        // TODO: stereotypes for operators like __add__, __eq__, etc
        if (name.equals("__init__"))
        {
            Object stereotype = this.getStereotype(resultOperation, "create");
            if (stereotype != null)
            {
                Model.getExtensionMechanismsHelper().addCopyStereotype(
                        resultOperation, stereotype);
            }
        }
        else if (name.equals("__del__"))
        {
            Object stereotype = this.getStereotype(resultOperation, "destroy");
            if (stereotype != null)
            {
                Model.getExtensionMechanismsHelper().addCopyStereotype(
                        resultOperation, stereotype);
            }
        }

        // arguments
        for (PyArgument a : fd.getArguments())
        {
            Object parameter = this.coreFactory.createParameter();
            this.coreHelper.setName(parameter, a.getName());
            this.coreHelper.addParameter(resultOperation, parameter);

            // can't decide if "in" or "in out"?
            // assuming "in"
            this.coreHelper.setKind(parameter, Model.getDirectionKind()
                    .getInParameter());

            int argumentType = a.getArgumentType();
            switch (argumentType)
            {
                // TODO: apply stereotypes here
                case PyArgument.NORMAL:
                    break;
                case PyArgument.VARLENGTH:
                    break;
                case PyArgument.KEYWORD:
                    break;
                case PyArgument.TUPLE:
                    break;
            }

            PyExpression value = a.getDefaultValue();
            if (value != null)
            {
                // try to set initial value for parameter
                Object initialValue = null;

                // maybe the initial value gives us a type to use
                Object parameterType = null;

                if (value.isAtom())
                {
                    PyAtom atom = (PyAtom) value;
                    if (atom instanceof PyNumber)
                    {
                        PyNumber n = (PyNumber) atom;
                        switch (n.getType())
                        {
                            case PyNumber.INTEGER:
                                parameterType = this.intType;
                                break;
                            case PyNumber.LONG:
                                parameterType = this.longType;
                                break;
                            case PyNumber.COMPLEX:
                                // this should definitly come from a
                                // python-profile
                                break;
                            case PyNumber.FLOAT:
                                // uml has no floating point type :(
                                break;
                        }

                        initialValue = this.dataTypesFactory.createExpression(
                                "Python", n.getValue());
                    }
                }

                if (initialValue != null)
                {
                    this.coreHelper.setDefaultValue(parameter, initialValue);

                }

                if (parameterType != null)
                {
                    this.coreHelper.setType(parameter, parameterType);
                }
            }
        }

        // visibility
        if (!name.endsWith("__"))
        {
            VisibilityKind vk = Model.getVisibilityKind();
            if (name.startsWith("__"))
            {
                this.coreHelper.setVisibility(resultOperation, vk.getPrivate());
            }
            else if (name.startsWith("_"))
            {
                this.coreHelper.setVisibility(resultOperation, vk
                        .getProtected());
            }
            else
            {
                this.coreHelper.setVisibility(resultOperation, vk.getPublic());
            }
        }

        // function body
        // TODO: this should be optional
        if (!fd.isEmpty())
        {
            Function2Statemachine f2s = new Function2Statemachine();
            Object statemachine = f2s.toStatemachine(fd);
            if (statemachine != null)
            {
                StateMachinesHelper h = Model.getStateMachinesHelper();
                h.setContext(statemachine, resultOperation);
            }

        }

        return resultOperation;
    }

    private ArrayList<String> findParentModules(PyCompilationUnit cu)
    {
        ArrayList<String> namespace = new ArrayList<String>();
        File currentPath = cu.getFile().getParentFile();

        while (currentPath != null)
        {
            boolean initFound = false;
            for (File f : currentPath.listFiles())
            {
                if ("__init__.py".equals(f.getName()))
                {
                    namespace.add(0, currentPath.getName());
                    currentPath = currentPath.getParentFile();
                    initFound = true;
                    break;
                }

            }
            if (!initFound)
            {
                currentPath = null;
            }
        }

        return namespace;
    }

    private Object getOrCreateNamespace(ArrayList<String> namespace)
    {
        Object rootModel = this.modelManagementFactory.getRootModel();

        if (namespace.size() == 0)
        {
            return rootModel;
        }

        Object currentNamespace = rootModel;
        for (String currentName : namespace)
        {
            Collection<Object> namespaces = this.modelManagementHelper
                    .getAllNamespaces(currentNamespace);
            boolean found = false;

            for (Object o : namespaces)
            {
                String tmpName = this.facade.getName(o);
                if (tmpName.equals(currentName))
                {
                    currentNamespace = o;
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                Object newNamespace = this.modelManagementFactory
                        .createPackage();
                this.coreHelper.setName(newNamespace, currentName);
                this.coreHelper.setNamespace(newNamespace, currentNamespace);
                this.createdObjects.add(newNamespace);
                currentNamespace = newNamespace;
            }
        }

        return currentNamespace;
    }

    private Object getStereotype(Object o, String name)
    {
        for (Object stereo : StereotypeUtility.getAvailableStereotypes(o))
        {
            if (name.equals(this.facade.getName(stereo)))
            {
                return stereo;
            }
        }
        return null;
    }
}