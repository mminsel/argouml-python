// $Id: Model2PyElement.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package org.argouml.python.transform;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.argouml.model.Facade;
import org.argouml.model.MetaTypes;
import org.argouml.model.Model;
import org.argouml.python.py.PyAttribute;
import org.argouml.python.py.PyClass;
import org.argouml.python.py.PyFunction;
import org.argouml.python.py.PyModule;
import org.argouml.python.py.PyVariable;

public class Model2PyElement
{
    // python's basic data-types
    private static Set<String> vartypes = new HashSet<String>();
    static
    {
        Model2PyElement.vartypes.add("void");
        Model2PyElement.vartypes.add("boolean");
        Model2PyElement.vartypes.add("int");
        Model2PyElement.vartypes.add("integer"); 
        Model2PyElement.vartypes.add("long");
        Model2PyElement.vartypes.add("float");
        Model2PyElement.vartypes.add("double");
        Model2PyElement.vartypes.add("string");
    }

    // convenience, used in every method
    private Facade f;

    private Set<Object> dependencies = new HashSet<Object>();
    private boolean solveDeps;

    private Set<Object> handledObjects = new HashSet<Object>();

    private Logger log;

    public Model2PyElement(boolean solveDeps)
    {
        this.f = Model.getFacade();
        this.solveDeps = solveDeps;
        this.log = Logger.getLogger(this.getClass());
    }

    public Collection<PyModule> toPyModules(Collection objs) throws Exception
    {
        HashMap<String, PyModule> modules = new HashMap<String, PyModule>();
        MetaTypes metaTypes = Model.getMetaTypes();

        for (Object currentObject : objs) {

            // get full namespace
            ArrayList<String> namespaces = new ArrayList<String>();
            Object n = f.getNamespace(currentObject);

            while (n != null)
            {
                String name = f.getName(n);
                if (name != null && name != "")
                {
                    namespaces.add(0, name); 
                }

                n = f.getNamespace(n);
            }

            // find module by namespace
            // if it doesn't exists, it will be created
            String nstr = namespaces.get(0);
            PyModule current = modules.get(nstr);
            if (current == null)
            {
                current = new PyModule(nstr);
                modules.put(nstr, current);
            }

            for (int i = 1; i < namespaces.size(); ++i)
            {
                nstr = namespaces.get(i);
                PyModule tmp = current.getSubModule(nstr);
                if (tmp == null)
                {
                    tmp = new PyModule(nstr);
                    current.add(tmp);
                }

                current = tmp;
            }

            String metaType = metaTypes.getName(currentObject);

            if (metaType.equals("Class"))
            {
                PyClass pc = null;

                try
                {
                    pc = this.toPyClass(currentObject);
                }
                catch (Exception e)
                {
                    this.log.error("while transforming: " + e);
                }

                if (pc != null)
                {
                    current.add(pc);
                }
            }
            else
            {
                // there should be no other types here
                // this may change, we will see
                this.log.warn("why is a " + metaType + " here?");
            }
        }

        return modules.values();
    }

    public PyClass toPyClass(Object o) throws Exception
    {
        if (this.handledObjects.contains(o))
        {
            return null;
        }

        PyClass pc = new PyClass(this.f.getName(o));

        // base classes
        for (Object g : f.getGeneralizations(o)) {
            PyClass bc = new PyClass(this.f.getName(o));
            pc.addBaseClass(bc);
        }

        // attributes
        for (Object a : f.getAttributes(o)) {
            PyAttribute pa;
            
            try
            {
                pa = this.toPyAttribute(a);

            }
            catch (Exception e)
            {
                this.log.error("while transforming: " + e);
                continue;
            }

            if (pa != null)
            {
                pc.add(pa);
            }
        }
        

        // methods
        for (Object op : f.getOperations(o)) {
            PyFunction pf;
            try
            {
                pf = this.toPyFunction(op);

            }
            catch (Exception e)
            {
                this.log.error("while transforming: " + e);
                continue;
            }

            if (pf != null)
            {
                pc.add(pf);
            }
        }

        this.handledObjects.add(o);

        return pc;
    }

    public PyFunction toPyFunction(Object o) throws Exception
    {
        if (this.handledObjects.contains(o))
        {
            return null;
        }

        PyFunction pf;

        if (this.f.isConstructor(o))
        {
            pf = new PyFunction("__init__");
        }
        else
        {
            pf = new PyFunction(this.f.getName(o));
        }

        pf.isMethod(true);

        for (Object arg : f.getParameters(o)) {
            if (this.f.isReturn(arg))
            {
                // add some text to docstring of method
                continue;
            }

            PyVariable pv = null;
            try
            {
                pv = this.toPyVariable(arg);

            }
            catch (Exception e)
            {
                // log
                this.log.error("while transforming: " + e);
            }

            if (pv != null)
            {
                pf.add(pv);
            }
        }

        this.handledObjects.add(o);

        return pf;
    }
    
    public PyVariable toPyVariable(Object o) throws Exception
    {
        if (this.handledObjects.contains(o))
        {
            return null;
        }

        PyVariable pv = new PyVariable(this.f.getName(o));

        Object vartype = this.f.getType(o);
        if (vartype != null) 
        {
            String name = this.f.getName(vartype);
            if (!Model2PyElement.vartypes.contains(name))
            {
                this.dependencies.add(vartype);
            }
        }

        Object defaultValue = this.f.getDefaultValue(o);
        if (defaultValue != null)
        {
            String dValueString = (String) this.f.getBody(defaultValue);
            // TODO: getLanguage(defaultValue)!!!
            pv.setInitialValue(dValueString);
        }

        this.handledObjects.add(o);

        return pv;
    }
    
    public PyAttribute toPyAttribute(Object o) throws Exception
    {
        if (this.handledObjects.contains(o))
        {
            return null;
        }

        PyAttribute pa = new PyAttribute(this.f.getName(o));

        if (this.f.isPrivate(o))
        {
            pa.isPrivate(true);
        }
        else if (this.f.isProtected(o))
        {
            pa.isProtected(true);
        }

        if (this.f.isStatic(o))
        {
            pa.isClassAttribute(true);
        }

        Object vartype = this.f.getType(o);
        if (vartype != null) 
        {
            String name = this.f.getName(vartype);
            pa.setVariableType(name);
            if (!Model2PyElement.vartypes.contains(name))
            {
                this.dependencies.add(vartype);
            }
        }

        Object initialValue = this.f.getInitialValue(o);
        if (initialValue != null)
        {
            String dValueString = (String) this.f.getBody(initialValue);
            // TODO: getLanguage(initialValue)!!!
            pa.setInitialValue(dValueString);
        }

        this.handledObjects.add(o);

        return pa;
    }
}
