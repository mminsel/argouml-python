// $Id: Model2SourceUnits.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.argouml.python.transform;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.argouml.python.py.PyCompilationUnit;
import org.argouml.uml.generator.SourceUnit;

public class Model2SourceUnits
{
    private boolean solveDeps;
    private String path;

    private Logger log;

    public Model2SourceUnits(String path, boolean solveDeps)
    {
        this.path = path;
        this.solveDeps = solveDeps;
        this.log = Logger.getLogger(this.getClass());
    }

    public Collection<SourceUnit> toSourceUnits(Collection objs)
    {
        ArrayList<SourceUnit> res = new ArrayList<SourceUnit>();

        Model2CompilationUnit t = new Model2CompilationUnit(this.path,
                this.solveDeps);

        Collection<PyCompilationUnit> cunits;

        try
        {
            cunits = t.toPyCompilationUnits(objs);
        }
        catch (Exception e)
        {
            this.log.error("can't transform", e);
            return res;
        }

        for (PyCompilationUnit cu : cunits)
        {
            try
            {
                CompilationUnit2Code t2 = new CompilationUnit2Code();
                String code = t2.toCode(cu);
                File cuFile = cu.getFile();
                String filename = cuFile.getName();
                String path = cuFile.getParentFile().getAbsolutePath();
                SourceUnit su = new SourceUnit(filename, path, code);
                res.add(su);
            }
            catch (Exception e)
            {
                this.log.error("can't transform: " + e);
            }
        }

        return res;
    }
}
