// $Id: Function2Statemachine.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.argouml.python.transform;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.argouml.model.CommonBehaviorFactory;
import org.argouml.model.CommonBehaviorHelper;
import org.argouml.model.CoreFactory;
import org.argouml.model.CoreHelper;
import org.argouml.model.DataTypesFactory;
import org.argouml.model.DataTypesHelper;
import org.argouml.model.Facade;
import org.argouml.model.Model;
import org.argouml.model.ModelManagementFactory;
import org.argouml.model.ModelManagementHelper;
import org.argouml.model.PseudostateKind;
import org.argouml.model.StateMachinesFactory;
import org.argouml.model.StateMachinesHelper;
import org.argouml.python.py.PyBreakStatement;
import org.argouml.python.py.PyConditionalStatement;
import org.argouml.python.py.PyContinueStatement;
import org.argouml.python.py.PyExpression;
import org.argouml.python.py.PyFunctionDefinition;
import org.argouml.python.py.PyReturnStatement;
import org.argouml.python.py.PyStatement;
import org.argouml.python.py.PyWhileLoop;

public class Function2Statemachine
{
    private Logger log;
    private CoreFactory coreFactory;
    private CoreHelper coreHelper;
    private ModelManagementFactory modelManagementFactory;
    private ModelManagementHelper modelManagementHelper;
    private StateMachinesFactory stateMachinesFactory;
    private StateMachinesHelper stateMachinesHelper;
    private CommonBehaviorFactory commonBehaviorFactory;
    private CommonBehaviorHelper commonBehaviorHelper;
    private PseudostateKind stateKinds;
    private DataTypesFactory dataTypesFactory;
    private DataTypesHelper dataTypesHelper;
    private Facade facade;
    private Stack<Object> loops;
    private List<Object> pendingBreaks;
    private Object pendingGuardExpression;

    public Function2Statemachine()
    {
        this.log = Logger.getLogger(this.getClass());

        this.coreFactory = Model.getCoreFactory();
        this.coreHelper = Model.getCoreHelper();
        this.modelManagementFactory = Model.getModelManagementFactory();
        this.modelManagementHelper = Model.getModelManagementHelper();
        this.stateMachinesFactory = Model.getStateMachinesFactory();
        this.stateMachinesHelper = Model.getStateMachinesHelper();
        this.stateKinds = Model.getPseudostateKind();
        this.commonBehaviorFactory = Model.getCommonBehaviorFactory();
        this.commonBehaviorHelper = Model.getCommonBehaviorHelper();
        this.dataTypesFactory = Model.getDataTypesFactory();
        this.dataTypesHelper = Model.getDataTypesHelper();
        this.facade = Model.getFacade();
        this.loops = new Stack<Object>();
        this.pendingBreaks = new ArrayList<Object>();
        this.pendingGuardExpression = null;
    }

    public Object toStatemachine(PyFunctionDefinition fd)
    {
        // function body
        Object stateMachine = this.stateMachinesFactory.createStateMachine();

        Object machineState = this.stateMachinesFactory
                .buildCompositeStateOnStateMachine(stateMachine);

        // initial state
        Object initialState = this.stateMachinesFactory
                .buildPseudoState(machineState);
        this.coreHelper.setKind(initialState, this.stateKinds.getInitial());

        // final state, will be created later(if needed)
        Object finalState = null;

        List<Object> firstStates = new ArrayList<Object>();
        List<Object> lastStates = new ArrayList<Object>();
        lastStates.add(initialState);

        for (PyStatement s : fd.getBody())
        {
            List<List<Object>> nextFirstLast = this.toStatemachine(s,
                    machineState);

            if (nextFirstLast == null)
            {
                this.log.warn("statement " + s
                        + " returned null for first-last");
                continue;
            }

            firstStates = nextFirstLast.get(0);
            for (Object dest : firstStates)
            {
                for (Object source : lastStates)
                {
                    Object trans = this.stateMachinesFactory.buildTransition(
                            machineState, source, dest);
                }
            }
            
            lastStates = nextFirstLast.get(1);
        }

        for (Object source : lastStates)
        {
            if (source == initialState)
            {
                // no states where added
                break;
            }
            else
            {
                if (finalState == null)
                {
                    finalState = this.stateMachinesFactory
                            .buildFinalState(machineState);
                }
                Object trans = this.stateMachinesFactory.buildTransition(
                        machineState, source, finalState);

                if (this.pendingGuardExpression != null)
                {
                    Object guard = this.stateMachinesFactory.buildGuard(trans);
                    this.stateMachinesHelper.setExpression(guard,
                            this.pendingGuardExpression);
                    this.pendingGuardExpression = null;
                }
            }
        }
        return stateMachine;
    }

    // switches to different handler-methods for subclasses of PyStatement
    public List<List<Object>> toStatemachine(PyStatement s,
            Object compositeState)
    {
        if (s instanceof PyConditionalStatement)
        {
            return this.toStatemachine((PyConditionalStatement) s,
                    compositeState);
        }
        else if (s instanceof PyReturnStatement)
        {
            return this.toStatemachine((PyReturnStatement) s, compositeState);
        }
        else if (s instanceof PyBreakStatement)
        {
            return this.toStatemachine((PyBreakStatement) s, compositeState);
        }
        else if (s instanceof PyContinueStatement)
        {
            return this.toStatemachine((PyContinueStatement) s, compositeState);
        }
        else if (s instanceof PyWhileLoop)
        {
            return this.toStatemachine((PyWhileLoop) s, compositeState);
        }
        else if (s instanceof PyExpression)
        {
            return this.toStatemachine((PyExpression) s, compositeState);
        }
        // could not be handled
        this.log.error("was unable to handle " + s);

        return null;
    }

    public List<List<Object>> toStatemachine(
            PyExpression e, Object containerState)
    {
        Object state = this.stateMachinesFactory
                .buildSimpleState(containerState);
        this.coreHelper.setName(state, "Expression");

        Object expressionAction = this.commonBehaviorFactory
                .buildUninterpretedAction(state);
        Object expressionExpression = this.dataTypesFactory
                .createActionExpression("Python", e.toString());
        this.commonBehaviorHelper.setScript(expressionAction,
                expressionExpression);

        this.stateMachinesHelper.setDoActivity(state, expressionAction);

        // i need to return values here, an array of a generic doesn't seem to
        // be allowed :(
        List<List<Object>> result = 
            new ArrayList<List<Object>>();

        List<Object> firstStates = new ArrayList<Object>();
        firstStates.add(state);
        result.add(firstStates);

        List<Object> lastStates = new ArrayList<Object>();
        lastStates.add(state);
        result.add(lastStates);

        return result;
    }

    public List<List<Object>> toStatemachine(PyReturnStatement s,
            Object containerState)
    {
        Object state = this.stateMachinesFactory
                .buildFinalState(containerState);
        this.coreHelper.setName(state, "Return");

        Object returnAction = this.commonBehaviorFactory.createReturnAction();
        Object returnExpression = this.dataTypesFactory.createActionExpression(
                "Python", s.toString());
        this.commonBehaviorHelper.setScript(returnAction, returnExpression);

        this.stateMachinesHelper.setDoActivity(state, returnAction);

        // i need to return values here, an array of a generic doesn't seem to
        // be allowed :(
        List<List<Object>> result = new ArrayList<List<Object>>();

        List<Object> firstStates = new ArrayList<Object>();
        firstStates.add(state);
        result.add(firstStates);

        // final states must not have outgoing transitions
        // so this one is left empty
        List<Object> lastStates = new ArrayList<Object>();
        result.add(lastStates);

        return result;
    }

    public List<List<Object>> toStatemachine(PyConditionalStatement cs,
            Object containerState)
    {
        // condition state
        Object ifConditionState = this.stateMachinesFactory
                .buildPseudoState(containerState);
        this.coreHelper.setKind(ifConditionState, this.stateKinds.getChoice());
        this.coreHelper.setName(ifConditionState, "If");

        // save these for later
        List<Object> allLastStates = new ArrayList<Object>();

        List<Object> firstStates = new ArrayList<Object>();
        List<Object> lastStates = new ArrayList<Object>();
        lastStates.add(ifConditionState);

        Object lastTransition = null;

        // then path
        for (PyStatement s : cs.getThenStatements())
        {
            List<List<Object>> nextFirstLast = this.toStatemachine(s,
                    containerState);

            if (nextFirstLast == null)
            {
                this.log.warn("statement " + s
                        + " returned null for first-last");
                continue;
            }

            firstStates = nextFirstLast.get(0);
            for (Object dest : firstStates)
            {
                for (Object source : lastStates)
                {
                    Object tmp = this.stateMachinesFactory.buildTransition(
                            containerState, source, dest);

                    // we save the first transition to apply the guard later
                    if (lastTransition == null)
                    {
                        lastTransition = tmp;
                    }
                }
            }
            lastStates = nextFirstLast.get(1);
        }

        // add last states from then path
        allLastStates.addAll(lastStates);

        // set condition guard
        if (lastTransition == null)
        {
            this.log.error("no destination for if-condition");
        }
        else
        {
            PyExpression condition = cs.getCondition();
            String conditionString = condition.toString();
            Object expression = this.dataTypesFactory.createBooleanExpression(
                    "Python", conditionString);

            Object guard = this.stateMachinesFactory.buildGuard(lastTransition);
            this.stateMachinesHelper.setExpression(guard, expression);
        }

        // reset this variable
        lastTransition = null;

        // will be used later to connect else-path
        Object lastCondition = ifConditionState;

        // elif's blocks
        // if they exist set up a "else" guard to point to the first elif
        for (PyConditionalStatement c : cs.getElseIfStatements())
        {
            List<List<Object>> nextFirstLast = this.toStatemachine(c,
                    containerState);

            if (nextFirstLast == null)
            {
                this.log.warn("statement " + c
                        + " returned null for first-last");
                continue;
            }

            firstStates = nextFirstLast.get(0);

            // link condition states
            Object nextConditionState = firstStates.get(0);
            Object nextConditionTrans = this.stateMachinesFactory
                    .buildTransition(containerState, lastCondition,
                            nextConditionState);

            Object nextConditionExpression = this.dataTypesFactory
                    .createBooleanExpression("Python", "else");

            Object nextConditionGuard = this.stateMachinesFactory
                    .buildGuard(nextConditionTrans);
            this.stateMachinesHelper.setExpression(nextConditionGuard,
                    nextConditionExpression);

            lastCondition = nextConditionState;

            for (Object dest : firstStates)
            {
                for (Object source : lastStates)
                {
                    Object tmp = this.stateMachinesFactory.buildTransition(
                            containerState, source, dest);

                    // we save the first transition to apply the guard later
                    // also the state to connect else-path
                    if (lastTransition == null)
                    {
                        lastTransition = tmp;
                    }
                }
            }
            lastStates = nextFirstLast.get(1);
            allLastStates.addAll(lastStates);

            // set "else" guard for elif
            if (lastTransition != null)
            {
                Object expression = this.dataTypesFactory
                        .createBooleanExpression("Python", "else");

                Object guard = this.stateMachinesFactory
                        .buildGuard(lastTransition);
                this.stateMachinesHelper.setExpression(guard, expression);
                lastTransition = null;
            }
        }

        // else block
        lastStates.clear();
        if (lastCondition == null)
        {
            lastStates.add(ifConditionState);
        }
        else
        {
            lastStates.add(lastCondition);
        }

        for (PyStatement s : cs.getElseStatements())
        {
            List<List<Object>> nextFirstLast = this.toStatemachine(s,
                    containerState);

            if (nextFirstLast == null)
            {
                this.log.warn("statement " + s
                        + " returned null for first-last");
                continue;
            }

            firstStates = nextFirstLast.get(0);
            for (Object dest : firstStates)
            {
                for (Object source : lastStates)
                {
                    Object tmp = this.stateMachinesFactory.buildTransition(
                            containerState, source, dest);

                    // we save the first transition to apply the guard later
                    if (lastTransition == null)
                    {
                        lastTransition = tmp;
                    }
                }
            }
            lastStates = nextFirstLast.get(1);
        }

        allLastStates.addAll(lastStates);

        // set "else" guard for else-block
        if (lastTransition == null)
        {
            this.log.warn("no transition for else");
        }
        else
        {
            Object expression = this.dataTypesFactory.createBooleanExpression(
                    "Python", "else");

            Object guard = this.stateMachinesFactory.buildGuard(lastTransition);
            this.stateMachinesHelper.setExpression(guard, expression);
        }
        // add last states from then path

        List<List<Object>> result = new ArrayList<List<Object>>();

        List<Object> firstConditionStates = new ArrayList<Object>();
        firstConditionStates.add(ifConditionState);
        result.add(firstConditionStates);

        result.add(allLastStates);

        return result;
    }

    public List<List<Object>> toStatemachine(PyWhileLoop wl,
            Object containerState)
    {
        // condition state
        Object whileConditionState = this.stateMachinesFactory
                .buildPseudoState(containerState);
        this.coreHelper.setKind(whileConditionState, this.stateKinds
                .getChoice());
        this.coreHelper.setName(whileConditionState, "While");

        List<Object> firstStates = new ArrayList<Object>();
        List<Object> lastStates = new ArrayList<Object>();
        lastStates.add(whileConditionState);

        this.loops.push(whileConditionState);

        Object lastTransition = null;

        // then path
        for (PyStatement s : wl.getBody())
        {
            List<List<Object>> nextFirstLast = this.toStatemachine(s,
                    containerState);

            if (nextFirstLast == null)
            {
                this.log.warn("statement " + s
                        + " returned null for first-last");
                continue;
            }

            firstStates = nextFirstLast.get(0);
            for (Object dest : firstStates)
            {
                for (Object source : lastStates)
                {
                    Object tmp = this.stateMachinesFactory.buildTransition(
                            containerState, source, dest);

                    // we save the first transition to apply the guard later
                    if (lastTransition == null)
                    {
                        lastTransition = tmp;
                    }
                }
            }
            lastStates = nextFirstLast.get(1);
        }

        // set condition guard
        if (lastTransition == null)
        {
            this.log.error("no destination for while-condition");
        }
        else
        {
            PyExpression condition = wl.getCondition();
            String conditionString = condition.toString();
            Object expression = this.dataTypesFactory.createBooleanExpression(
                    "Python", conditionString);

            Object guard = this.stateMachinesFactory.buildGuard(lastTransition);
            this.stateMachinesHelper.setExpression(guard, expression);
        }

        this.loops.pop();

        // result
        List<List<Object>> result = new ArrayList<List<Object>>();
        List<Object> resultFirstStates = new ArrayList<Object>();
        resultFirstStates.add(whileConditionState);
        result.add(resultFirstStates);

        // else guard expression
        Object expression = this.dataTypesFactory.createBooleanExpression(
                "Python", "else");

        if (this.pendingBreaks.size() > 0)
        {
            // in case of existing break we add a last state
            // to route them to it. the else-transition has to go there, too
            Object lastState = this.stateMachinesFactory
                    .buildPseudoState(containerState);
           
            Object trans = this.stateMachinesFactory.buildTransition(
                    containerState, whileConditionState, lastState);
            Object guard = this.stateMachinesFactory.buildGuard(trans);
            this.stateMachinesHelper.setExpression(guard, expression);

            for (Object b : this.pendingBreaks)
            {
                this.stateMachinesFactory.buildTransition(containerState, b,
                        lastState);
            }
            
            List<Object> resultLastStates = new ArrayList<Object>();
            result.add(resultLastStates);
        }
        else
        {
            // condition is first and last
            result.add(resultFirstStates);

            // else-guard is pending
            this.pendingGuardExpression = expression;
        }

        return result;
    }
    
    public List<List<Object>> toStatemachine(PyBreakStatement s,
            Object containerState)
    {
        Object state = 
            this.stateMachinesFactory.buildPseudoState(containerState);
        this.coreHelper.setName(state, "Break");
        this.pendingBreaks.add(state);
        
        List<List<Object>> result = new ArrayList<List<Object>>();
        List<Object> firstStates = new ArrayList<Object>();
        firstStates.add(state);
        result.add(firstStates);
        result.add(new ArrayList<Object>());
        
        return result;
    }
    
    public List<List<Object>> toStatemachine(PyContinueStatement s, 
            Object containerState)
    {
        Object state = 
            this.stateMachinesFactory.buildPseudoState(containerState);
        this.coreHelper.setName(state, "Continue");
        
        if (this.loops.size() > 0)
        {
            Object loop = this.loops.peek();
            this.stateMachinesFactory.buildTransition(containerState, state, 
                    loop);
        }
        else
        {
            this.log.error("no loop for continue-statement");
        }
        
        List<List<Object>> result = new ArrayList<List<Object>>();
        List<Object> firstStates = new ArrayList<Object>();
        firstStates.add(state);
        result.add(firstStates);
        result.add(new ArrayList<Object>());
        
        return result;
    }
}