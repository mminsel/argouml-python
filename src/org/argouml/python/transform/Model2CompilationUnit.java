// $Id: Model2CompilationUnit.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.argouml.python.transform;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.runtime.RecognitionException;
import org.apache.log4j.Logger;
import org.argouml.application.api.Argo;
import org.argouml.model.DataTypesHelper;
import org.argouml.model.Facade;
import org.argouml.model.Model;
import org.argouml.model.VisibilityKind;
import org.argouml.python.py.PyArgument;
import org.argouml.python.py.PyAssignmentExpression;
import org.argouml.python.py.PyAssociationDefinition;
import org.argouml.python.py.PyAttribute;
import org.argouml.python.py.PyClassDefinition;
import org.argouml.python.py.PyCollection;
import org.argouml.python.py.PyCompilationUnit;
import org.argouml.python.py.PyDecoratorStatement;
import org.argouml.python.py.PyExpression;
import org.argouml.python.py.PyFunctionDefinition;
import org.argouml.python.py.PyImportStatement;
import org.argouml.python.py.PyName;
import org.argouml.python.py.PyReturnStatement;
import org.argouml.python.py.PyHelpStatement;
import org.argouml.python.reveng.ExpressionParser;

public class Model2CompilationUnit
{
    private Set<Object> dependencies = new HashSet<Object>();
    private boolean solveDeps;
    private String path;
    private Set<Object> handledObjects = new HashSet<Object>();
    private Logger log;
    private Facade f;
    private Map moduleInits;
    private DataTypesHelper dth;

    public Model2CompilationUnit(String path, boolean solveDeps)
    {
        this.f = Model.getFacade();
        this.dth = Model.getDataTypesHelper();

        this.solveDeps = solveDeps;
        this.path = path;
        this.log = Logger.getLogger(this.getClass());
        this.moduleInits = new HashMap<String, PyCompilationUnit>();
    }

    public Collection<PyCompilationUnit> toPyCompilationUnits(Collection objs)
            throws Exception
    {
        List<PyCompilationUnit> result = new ArrayList<PyCompilationUnit>();

        // we only get classes here
        for (Object o : objs)
        {
            PyCompilationUnit u = this.toPyCompilationUnit(o);
            if (u != null)
            {
                result.add(u);
            }
        }

        return result;
    }

    public PyCompilationUnit toPyCompilationUnit(Object o)
    {
        String sep = System.getProperty("file.separator");

        String name = this.f.getName(o);

        // TODO: name == null -> some random-temp-name
        ArrayList<String> filePathElements = this.getFullNamespace(o, false);
        filePathElements.add(0, this.path);
        filePathElements.remove(filePathElements.size()-1);
        
        File resultFile = new File(String.join(sep, filePathElements) + ".py");
        
        PyCompilationUnit result = new PyCompilationUnit(resultFile);

        PyClassDefinition c = this.toPyClassDefinition(o);
        result.addStatement(c);

        return result;

    }
    
    /*
     * this method creates an array of strings holding all namespaces of the current object.
     * in addition it adds the non-recursive object twice at the end of the list.
     * the last entry represents the element and the one before the module name of the element.
     * 
     * param o: object should be a PyClass object
     * param isRecurse: boolean. defines whether this function was called in recursion or not.
     *                  this param should always be false when called manually unless the moduleName
     *                  should not be added to the last entry
     * */
    private ArrayList<String> getFullNamespace(Object o, boolean isRecurse)
    {
        Object ns = this.f.getNamespace(o);
        if (ns == null)
        {
            ArrayList<String> moduleList = new ArrayList<String>();
            moduleList.add(this.f.getName(o));
            return moduleList;
        }
        ArrayList<String> moduleList = this.getFullNamespace(ns, true);
        String name =  this.f.getName(o);
        if (!isRecurse)
        {
            moduleList.add(name.substring(0, 1).toLowerCase() + name.substring(1));
        }
        moduleList.add(name);
        return moduleList;
    }

    
    public PyHelpStatement toPyHelpStatement(Object o)
    {
        /* Analyze a given object for documentation tags and
         * 
         * create a PyDocumentationStatement for it.
         * */
        PyHelpStatement result = new PyHelpStatement();
        
        Iterator iter = this.f.getTaggedValues(o);
        if (iter != null) {
            while (iter.hasNext()) {
                Object tv = iter.next();
                String tag = this.f.getTagOfTag(tv);
                
                if (tag == Argo.DOCUMENTATION_TAG)
                {
                    result.setBody(this.f.getValueOfTag(tv));
                } else if (tag == Argo.AUTHOR_TAG)
                {
                    result.setAuthor(this.f.getValueOfTag(tv));
                } else if (tag == Argo.VERSION_TAG)
                {
                    result.setVersion(this.f.getValueOfTag(tv));
                } else if (tag == Argo.SINCE_TAG)
                {
                    result.setSince(this.f.getValueOfTag(tv));
                } else if (tag == Argo.SEE_TAG)
                {
                    result.setSee(this.f.getValueOfTag(tv));
                } else if (tag == Argo.DEPRECATED_TAG)
                {
                    result.setDeprecated(
                                this.f.getValueOfTag(tv).toLowerCase().contentEquals("true")
                            );
                }
            }
        }
        
        
        //String docStr = DocumentationManager.getDocs(o, "", "'''", "\t", "'''");
        
        return result;
    }
    
    public PyFunctionDefinition toPyFunctionDefinition(Object operation)
    {
        PyFunctionDefinition result = new PyFunctionDefinition();
        
        
        /* get function name
         * 
         * in the following the name of the function is changed
         * according to its stereotype and visibility settings.
         *  */
        String operationName = this.f.getName(operation);
        String proposedName = null;
        
        
        // visibility
        Object operationVisibility = this.f.getVisibility(operation);
        
        VisibilityKind vk = Model.getVisibilityKind();
        
        /* match private functions to the __* pattern and 
         * protected functions to the _* pattern
         * public functions should neither start nor end with an underscore
         * */
        if (operationVisibility == vk.getPrivate())
        {
            
            if (!operationName.startsWith("__"))
            {
                if (!operationName.startsWith("_"))
                {
                    proposedName = "__" + operationName;
                }
                else
                {
                    proposedName = "_" + operationName;
                }
            }
        }
        else if (operationVisibility == vk.getProtected())
        {
            if (!operationName.startsWith("_"))
            {
                proposedName = "_" + operationName;
            }
        }
        else
        {
            /* in case a non private function starts with an _ remove those */
            if (operationName.startsWith("__"))
            {
                proposedName = operationName.substring(2);
            } else if (operationName.startsWith("_")) {
                proposedName = operationName.substring(1);
            }
        }
        
        /* in case the current operation is a constructor rename it to __init__ */
        if (this.f.isConstructor(operation))
        {
            proposedName = "__init__";
        }
        
        /* in case we changed the name log a warning */
        if (proposedName != null)
        {
            this.log.warn("in class '" + this.f.getName(operation)
                    + "': renaming operation '" + operationName
                    + "' due to visibilty.");
            operationName = proposedName;
        }
        result.setName(operationName);
        
        /* setup static functions.. */
        if (!this.f.isStatic(operation))
        {
            /* if not add the self argument */
            PyArgument selfArg = new PyArgument("self");
            result.addArgument(selfArg);
        } else {
            /* if yes add the staticmethod decorator and leave the self arg */
            PyDecoratorStatement dec = new PyDecoratorStatement();
            dec.setValue(PyDecoratorStatement.DecoratorStore.STATICMETHOD);
            result.addDecorator(dec);
        }
        

        /* analyze the functions parameter */
        
        /* create a default return */
        PyReturnStatement ret = null;
        
        for (Object parameter : this.f.getParametersList(operation))
        {
            String parameterName = this.f.getName(parameter);
            if (parameterName.contentEquals("self"))
            {
                continue;
            }
            Object iv = this.f.getDefaultValue(parameter);
            

            PyExpression initialValueExpression = null;
            if (iv != null)
            {
                if (this.f.isAExpression(iv))
                {
                    String lang = this.dth.getLanguage(iv);
                    String body = this.dth.getBody(iv);

                    if ("Python".equals(lang) && body != null && !"".equals(body))
                    {
                        ExpressionParser p = new ExpressionParser();

                        try
                        {
                            initialValueExpression = p.toExpression(body);
                        }
                        catch (RecognitionException e)
                        {
                            this.log.error("class '" + this.f.getName(operation)
                                    + "', operation '" + operationName
                                    + "', parameter '" + parameterName
                                    + "', can't parse inital value: " + e);
                        }
                    }
                }
            }
            
            
            if (this.f.isReturn(parameter))
            {
                ret = new PyReturnStatement();
                Object def = this.f.getDefaultValue(parameter);
                Object typ = this.f.getType(parameter);
                
                if (def != null)
                {
                    String defName = this.f.toString(this.f.toString(def));
                    ret.setValue(defName);
                } else if (typ != null){
                    String typName = this.f.getName(typ);
                    ret.setValue("# type: " + typName + ".");
                }
                ret.setValue(initialValueExpression);
                continue;
            }
            
            PyArgument arg = new PyArgument(parameterName);
            if (initialValueExpression != null)
            {
                arg.setDefaultValue(initialValueExpression);
            }
            
            result.addArgument(arg);
        }
        result.addBodyStatement(this.toPyHelpStatement(operation));
        
        if (ret != null)
        {
            result.addBodyStatement(ret);
        }
        
        
        return result;
    }
    
    
    public PyClassDefinition toPyClassDefinition(Object o)
    {
        if (!this.f.isAClass(o))
        {
            this.log.error("not a class: " + o);
            return null;
        }
        
        ArrayList<PyImportStatement> imports = new ArrayList<PyImportStatement>();
        
        PyClassDefinition result = new PyClassDefinition();
        result.setName(this.f.getName(o));

        // set base classes
        List<String> bases = new ArrayList<String>();
        for (Object base : this.f.getGeneralizations(o))
        {
            Object parent = this.f.getGeneral(base);
            if (parent == null)
            {
                continue;
            }
            bases.add(this.f.getName(parent));
            // in case we found a generalization we need to add it to the imports of this class
            ArrayList<String> allNamespaces = this.getFullNamespace(parent, false);
            PyImportStatement stmnt = new PyImportStatement();
            stmnt.setValue(allNamespaces);
            imports.add(stmnt);
        }

        if (bases.size() == 0)
        {
            PyName b = new PyName("object");
            result.setBases(b);
        }
        else if (bases.size() == 1)
        {
            String base = bases.get(0);
            PyName b = new PyName(base);
            result.setBases(b);
        }
        else
        {
            PyCollection b = new PyCollection(PyCollection.TUPLE);
            for (String baseString : bases)
            {
                PyName baseName = new PyName(baseString);
                b.addElement(baseName);
            }

            result.setBases(b);
        }

        // create attributes
        List<PyAssignmentExpression> instanceAttributes = new ArrayList<PyAssignmentExpression>();

        for (Object attribute : this.f.getAttributes(o))
        {
            String attributeName = this.f.getName(attribute);
            Object attributeVisibility = this.f.getVisibility(attribute);
            String proposedName = null;

            // visibility
            VisibilityKind vk = Model.getVisibilityKind();

            if (attributeVisibility == vk.getPrivate())
            {
                if (!attributeName.startsWith("__"))
                {
                    if (!attributeName.startsWith("_"))
                    {
                        proposedName = "__" + attributeName;
                    }
                    else
                    {
                        proposedName = "_" + attributeName;
                    }
                }
            }
            else if (attributeVisibility == vk.getProtected())
            {
                if (!attributeName.startsWith("_"))
                {
                    proposedName = "_" + attributeName;
                }
            }

            if (proposedName != null)
            {
                this.log.warn("in class '" + this.f.getName(o)
                        + "': renaming attribute '" + attributeName
                        + "' due to visibilty.");
                attributeName = proposedName;
            }

            Object iv = this.f.getInitialValue(attribute);
            PyExpression initialValueExpression = null;
            if (iv != null)
            {
                if (this.f.isAExpression(iv))
                {
                    String lang = this.dth.getLanguage(iv);
                    String body = this.dth.getBody(iv);

                    if ("Python".equals(lang) && body != null && !"".equals(body))
                    {
                        ExpressionParser p = new ExpressionParser();

                        try
                        {
                            initialValueExpression = p.toExpression(body);
                        }
                        catch (RecognitionException e)
                        {
                            this.log.error("class '" + this.f.getName(o)
                                    + "', attribute '" + attributeName
                                    + "', can't parse inital value: " + e);
                        }
                    }
                }

            }

            if (this.f.isStatic(attribute))
            {
                PyAssignmentExpression e = new PyAssignmentExpression();
                e.addTarget(new PyName(attributeName));
                if (initialValueExpression != null)
                {
                    e.setValue(initialValueExpression);
                }
                else
                {
                    e.setValue(new PyName("None"));
                }

                result.addBodyStatement(e);
            }
            else
            {
                PyAssignmentExpression e = new PyAssignmentExpression();
                PyName self = new PyName("self");
                PyName attr = new PyName(attributeName);
                attr.setMemberOf(self);
                e.addTarget(attr);

                if (initialValueExpression != null)
                {
                    e.setValue(initialValueExpression);
                }
                else
                {
                    e.setValue(new PyName("None"));
                }

                instanceAttributes.add(e);
            }
        }
        
        
        /* make associations 
         * the following code collects associations, compilations and aggrgations and
         * adds those to the compilation unit */
        for (Object firstEnd : this.f.getAssociationEnds(o)) {
            Object association = this.f.getAssociation(firstEnd);
            
            for (Object otherEnd : this.f.getConnections(association)){
                if (firstEnd == otherEnd || !this.f.isNavigable(otherEnd)){
                    continue;
                }
                
                boolean addImport = false;
                Object multiplicity = this.f.getMultiplicity(otherEnd);
                String multi = this.f.getName(multiplicity);
                Object endType = this.f.getType(otherEnd);
                String endTypeName = this.f.getName(endType);
                Object namespace = this.f.getNamespace(endType);
                String endTypeNamespace = this.f.getName(namespace);
                
                PyName value = new PyName("None");
                if (multi.contentEquals("0..*"))
                {
                    value = new PyName("[]");
                } else if (multi.contentEquals("1..*"))
                {
                    value = new PyName("[ " + endTypeName + "() ]");
                    addImport = true;
                } else if (multi.contentEquals("1"))
                {
                    value = new PyName(endTypeName + "()");
                    addImport = true;
                } 
                
                ArrayList<String> allNamespaces = new ArrayList<String>();
                if (addImport)
                {
                    allNamespaces = this.getFullNamespace(endType, false);
                }
            
                String attributeName = this.f.getName(otherEnd);
                
                
                PyAssociationDefinition assoc = new PyAssociationDefinition(
                                attributeName, result, endTypeName,
                                allNamespaces, PyAssociationDefinition.multiplicityFromString(multi));
                
                result.addAssociationStatement(assoc);
                
            }
        }
        result.addImportStatement(imports);
        
        result.addBodyStatement(this.toPyHelpStatement(o));
        
        // create methods, if no constructor exists, create one
        // TODO: use stereotype to define constructor, just use
        // name == __init__ for this :\
        PyFunctionDefinition constructor = null;

        for (Object operation : this.f.getOperations(o))
        {
            PyFunctionDefinition fd = this.toPyFunctionDefinition(operation);
            if ("__init__".equals(this.f.getName(operation)) || this.f.isConstructor(operation))
            {
                constructor = fd;
                continue;
            }
            result.addBodyStatement(fd);
        }

        // setup constructor if none exists
        if (constructor == null)
        {
            constructor = new PyFunctionDefinition("__init__");
            PyArgument selfArg = new PyArgument("self");
            constructor.addArgument(selfArg);
        }
        PyName superCall = new PyName("super(" + result.getName() + ", self).__init__()");
        constructor.addBodyStatement(superCall);
        for (PyAssignmentExpression e : instanceAttributes)
        {
            constructor.addBodyStatement(e);
        }
        
        result.addBodyStatement(constructor);
        
        return result;
    }
}
