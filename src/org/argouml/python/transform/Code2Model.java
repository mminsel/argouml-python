// $Id: Code2Model.java 93 2010-01-12 19:40:42Z linus $
/*Copyright (C) 2008 alexander krohn
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.argouml.python.transform;

import java.io.File;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.argouml.python.py.PyCompilationUnit;
import org.argouml.python.reveng.File2CompilationUnit;

public class Code2Model
{
    private static final Logger LOG = Logger.getLogger(Code2Model.class);

    public Code2Model()
    {
    }

    public ArrayList<Object> toModel(File f)
    {
        File2CompilationUnit f2su = new File2CompilationUnit();

        PyCompilationUnit cu;
        try
        {
            cu = f2su.toCompilationUnit(f);
        }
        catch (Exception e)
        {
            StackTraceElement ste[] = e.getStackTrace();
            if (ste.length > 0)
            {
                StackTraceElement causingSte = ste[0];
                String cf = causingSte.getFileName();
                int cl = causingSte.getLineNumber();
                LOG.error("parsing error in file '" + f + "': " + e + "(" + cf
                        + ", " + cl + ")");
            }
            else
            {
                LOG.error("parsing error in file '" + f + "': " + e);
            }
            return null;
        }

        if (cu == null)
        {
            return null;
        }

        CompilationUnit2Model cu2m = new CompilationUnit2Model();
        Object module = cu2m.toModel(cu);
        return cu2m.getCreatedObjects();
    }
}
